import React , {useContext, useEffect} from 'react';
import './App.scss'
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";
import { MyThemeContext } from './contexts/MyThemeContext'
import BackTestContextProvider from './contexts/BackTestContext'
import Header from './components/Header'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import PriceChartPage from './components/PriceChartPage'
import Settings from './components/Settings'
import Home from './components/Home'
import Strategy from './components/StrategyCreation'

import FinanceTable from  './components/FinanceTable/FinanceTableGroup';
import CreateSchema from './components/CreateSchema/CreateSchema';


function App() {
  const theme = useContext(MyThemeContext)
  console.log(theme)
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyle theme={theme}/>
        <AppCon theme={theme}>
          <Header />
          <BackTestContextProvider>            
              <Switch>
                <Route exact path="/" component={Home}  />
                <Route path="/backtest" component={PriceChartPage}  />
                <Route path="/settings" component={Settings}  />
                <Route path="/strategy" component={Strategy}  />                              
                <Route path="/finance-table" component={ FinanceTable }  />

                <Route path="/create-schema" component={ CreateSchema }  />

              </Switch>              
          </BackTestContextProvider>
        </AppCon>
      </ThemeProvider>
    </BrowserRouter>
  );
}

const GlobalStyle = createGlobalStyle`
  body {
    padding: 0;
    margin: 0;
    box-sizing : border-box;
    background-color : ${props => props.theme.theme.bg};
    color : ${props => props.theme.theme.text};

  }
`;

const AppCon = styled.div`
  width : 100%;
`;


export default App;