import React,  { createContext, useState} from 'react'

export const StrategyContext = createContext();

const StrategyContextProvider = (props) => {


    return(
        <StrategyContext.Provider value={{
            
                                          }}
            >
            {props.children}
        </StrategyContext.Provider>
    )
}

export default StrategyContextProvider