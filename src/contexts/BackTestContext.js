import React,  { createContext, useState, useEffect } from 'react'
import {API} from '../config/config'

export const BackTestContext = createContext();

const initalTransactions = [
    {
        company : 1234,
        date : "2004-04-22",
        amount : 1234.00,
        quantity : 1122,
        id : 122222,
        buyOrSell : "buy"
    },
    {
        company : 12234,
        date : "2004-05-17",
        amount : 1234.00,
        quantity : 1122,
        id : 1288922,
        buyOrSell : "sell"
    },
    {
        company : 1234,
        date : "2004-06-19",
        amount : 99884.00,
        quantity : 1422,
        id : 124522,
        buyOrSell : "buy"
    }
]

const BackTestContextProvider = (props) => {
    const [activeKey, setActiveKey] = useState("1")
    const [selectedCompany, setSelectedCompany] = useState({companyId : null, companyName : null})
    const [dateRange, setDateRange] = useState({startingDate : "2000-01-01", endingDate : "2003-12-31"})
    const [startingAmount, setStartingAmount] = useState(100000);
    const [strategy, setStrategy] = useState("Manual")
    const [singleDate, setSingleDate] = useState("2000-06-30")
    const [backTestStarted, setBackTestStarted] = useState(false)
    const [transactions, setTransactions] = useState([])
    const [priceData, setPriceData] = useState(null)
    const [selectedDataPoint, setSelectedDataPoint] = useState(0)
    const [currentCash, setCurrentCash] = useState(100000)
    const [currentQuantity, setCurrentQuantity] = useState(0)
    const [backTestSuccess, setBackTestSuccess] = useState(false)
    const [backTestResults, setBackTestResults] = useState(null)
    const [loadingBackTestResults, setLoadingBackTestResults] = useState(false)

    const handleActiveKey = (id) => {
        setActiveKey(id)
    }

    const handleSelectedCompany = (company) => {
        console.log(company)
        setSelectedCompany({
            companyId : company.companyId,
            companyName : company.companyName
        })
    }

    const handleDateRange = (dateRange) => {
        setDateRange({
            startingDate : dateRange.startingDate,
            endingDate : dateRange.endingDate
        })
    }

    const handleStartingAmount = (amount) => {
        setStartingAmount(amount)
    }

    const handleStrategy = (strategy) => {
        setStrategy(strategy)
    }

    const handleSingleDate = (date) => {
        setSingleDate(date)
    }

    const handleBackTestStarted = (torf) => {
        setBackTestStarted(torf)
    }

    const handleBackTestSuccess = (torf) => {
        setBackTestSuccess(torf)
    }

    const handleTransactions = (action, trxn) => {
        console.log(action, trxn)
        let localCurrentCash;
        let localCurrentQuantity;
        if(action === "add"){
            if(trxn.buyOrSell === 'buy'){
                localCurrentCash = currentCash - trxn.amount
                localCurrentQuantity = currentQuantity + trxn.quantity
            } else {
                localCurrentCash = currentCash + trxn.amount
                localCurrentQuantity = currentQuantity - trxn.quantity
            }
            setCurrentCash(localCurrentCash)
            setCurrentQuantity(localCurrentQuantity)
            setTransactions([...transactions, trxn])
        } else if(action === "edit"){
            let itrxn;
            itrxn = trxn
            let editedTrxns = [];
            let virtualCurrentCash;
            let virtualCurrentQuantity;
        
            let prevTransaction  = transactions.filter((ptrxn) => (ptrxn.id === itrxn.id))[0]
            
            console.log(prevTransaction)
            if(prevTransaction.buyOrSell === 'buy'){
                virtualCurrentCash = currentCash + prevTransaction.amount
                virtualCurrentQuantity = currentQuantity - prevTransaction.quantity
            } else if(prevTransaction.buyOrSell === 'sell'){
                virtualCurrentCash = currentCash - prevTransaction.amount
                virtualCurrentQuantity = currentQuantity + prevTransaction.quantity
            }
        
            let E_currentCash;
            let E_currentQuantity
            if(itrxn.buyOrSell === 'buy'){
                E_currentCash = virtualCurrentCash - itrxn.amount
                E_currentQuantity = virtualCurrentQuantity + itrxn.quantity
            } else {
                E_currentCash = virtualCurrentCash + itrxn.amount
                E_currentQuantity = virtualCurrentQuantity - itrxn.quantity
            }
           
            transactions.map((strxn) => {
              if(strxn.id === itrxn.id){
                editedTrxns.push(itrxn)
              } else {
                editedTrxns.push(strxn)
              }
            })

            setCurrentCash(E_currentCash)
            setCurrentQuantity(E_currentQuantity)
            setTransactions(editedTrxns)

        } else if(action === "reset"){

            setTransactions([])
            setCurrentCash(100000)
            setCurrentQuantity(0)

        } else if(action === "delete"){
            let D_currentCash;
            let D_currentQuantity
            if(trxn.buyOrSell === 'buy'){
                D_currentCash = currentCash + trxn.amount
                D_currentQuantity = currentQuantity - trxn.quantity
            } else {
                D_currentCash = currentCash - trxn.amount
                D_currentQuantity = currentQuantity + trxn.quantity
            }
            let newTrxns = transactions.filter( (strxn) => {
                if(trxn.id === strxn.id){
                    return false
                } else {
                    return true
                }
            } )
            setTransactions(newTrxns)
            setCurrentCash(D_currentCash)
            setCurrentQuantity(D_currentQuantity)

        } else if(action === "send"){

            let transactions_send = [];

            transactions.map((S_trxn) => {
                transactions_send.push({
                    date : `${S_trxn.date} 00:00:00`,
                    action : S_trxn.buyOrSell.toUpperCase(),
                    quantity : S_trxn.quantity,
                    companyId : S_trxn.company.toString(),
                    exchange : 1
                })
            })
        
            //console.log(transactions_send)
            setLoadingBackTestResults(true)
            fetch(`${API}/manualbacktest`, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                //mode: 'no-cors', // no-cors, *cors, same-origin
                //cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                //credentials: 'omit', // include, *same-origin, omit
                headers: {
                  'Content-Type': 'application/json'
                  // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                //redirect: 'follow', // manual, *follow, error
                //referrer: 'no-referrer', // no-referrer, *client
                body: JSON.stringify({
                   transactions : transactions_send}
                )// body data type must match "Content-Type" header
              }).then((res) => console.log(res.json().then((resh) => {
                console.log(resh)
                setBackTestSuccess(true)
                setBackTestResults(resh)
                setLoadingBackTestResults(false)
                // this.setState({
                //     backtest_success : true,
                //     current_cash_value : Math.round(resh.current_cash_value),
                //     current_stock_value : Math.round(resh.current_stock_value),
                //     total_invested_amount  : Math.round(resh.total_invested_amount),
                //     xirr : (resh.xirr*100).toFixed(2),
                //     btApiData : resh
                // })
                // this.setState({
                //   apiData : resh,
                // })
              })
              )).catch((err) => console.log(err))
        }
    }


    const handleSelectedDataPoint = (index) => {
        setSelectedDataPoint(index)
    }


    const getPriceData = () => {
        // console.log({
        //     "companyId": this.state.selectedCompany,
        //     "startDate": `${this.state.startDate} 00:00:00`,
        //     "endDate": `${this.state.endDate} 00:00:00`,
        //     "exchange":1,
        //     "high":1,
        //     "low":1,
        //     "open":1,
        //     "vol":1,
        //     "val":1,
        //     "mar_cap":1,
        //     "clos":0
        // })
  
      fetch(`${API}/pricelist`, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        //mode: 'no-cors', // no-cors, *cors, same-origin
        //cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        //credentials: 'omit', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        //redirect: 'follow', // manual, *follow, error
        //referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(
          {
            companyId: selectedCompany.companyId,
            startDate: `${dateRange.startingDate} 00:00:00`,
            endDate : `${dateRange.endingDate} 00:00:00`,
            exchange:1,
            high:1,
            low:1,
            open:1,
            vol:1,
            val:1,
            mar_cap:1,
            clos:1
        }
        )// body data type must match "Content-Type" header
      }).then((res) => console.log(res.json().then((resh) => {
        console.log(resh)
        setPriceData(resh.data)
      })
      )).catch((err) => console.log(err))
    }

    useEffect(() => console.log("turrrr", selectedCompany, selectedDataPoint))
    
    return(
        <BackTestContext.Provider value={{activeKey : activeKey,
                                          handleActiveKey : handleActiveKey,
                                          selectedCompany : selectedCompany,
                                          handleSelectedCompany : handleSelectedCompany,
                                          dateRange : dateRange,
                                          handleDateRange : handleDateRange,
                                          startingAmount : startingAmount,
                                          handleStartingAmount : handleStartingAmount,
                                          strategy : strategy,
                                          handleStrategy : handleStrategy,
                                          singleDate : singleDate,
                                          handleSingleDate : handleSingleDate,
                                          backTestStarted : backTestStarted,
                                          handleBackTestStarted : handleBackTestStarted,
                                          transactions : transactions,
                                          handleTransactions : handleTransactions,
                                          getPriceData : getPriceData,
                                          priceData : priceData,
                                          selectedDataPoint : selectedDataPoint,
                                          handleSelectedDataPoint : handleSelectedDataPoint,
                                          currentQuantity : currentQuantity,
                                          currentCash : currentCash,
                                          backTestSuccess : backTestSuccess,
                                          handleBackTestSuccess : handleBackTestSuccess,
                                          backTestResults : backTestResults,
                                          loadingBackTestResults : loadingBackTestResults
                                          }}
            >
            {props.children}
        </BackTestContext.Provider>
    )
}


export default BackTestContextProvider