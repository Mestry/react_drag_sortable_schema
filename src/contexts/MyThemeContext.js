import React, { createContext, useState } from 'react'


export const MyThemeContext = createContext();


let SchemaPage = {
    page_border: 'red',
    color: '#fff'
}

const MyThemeContextProvider = (props) => {
    const [theme, setTheme] = useState({
        bg: "#2f5164",
        bgDark: "#11475c",
        text: "#ffffff",
        table_bg: "#102e42",
        table_row_title: "#fff",

        doubleBg: '#193d50',
        hoverColor: '#043f5f',    
        SchemaPage: SchemaPage,

    })

    const [currentTheme, setCurrentTheme] = useState({
        currentTheme: "dark"
    })

    const toggleTheme = (which) => {
        if (which === "dark") {
            setTheme({
                bg: "#2f5164",
                bgDark: "#11475c",
                text: "#ffffff",
                table_bg: "#102e42",
                table_row_title: "#ece794",

                doubleBg: '#193d50',
                hoverColor: '#043f5f',    
                SchemaPage: SchemaPage,                
            })
            setCurrentTheme({
                currentTheme: "dark"
            })
        } else if (which === "light") 
        {
            setTheme({
                bg: "#ffffff",
                bgDark: "#d5eaf5",
                text: "#2f5164",
                table_bg: "#fff",

                doubleBg : '#000',
                hoverColor: '#2489bf',
            })
            setCurrentTheme({
                currentTheme: "light"
            })
        }
    }

    return (
        <MyThemeContext.Provider value={{ theme: theme, currentTheme: currentTheme, toggleTheme: toggleTheme }}>
            {props.children}
        </MyThemeContext.Provider>
    )

}

export default MyThemeContextProvider