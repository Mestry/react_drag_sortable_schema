import React ,{ Component , } from 'react';
import { createPortal } from 'react-dom';

import './PopupModal.scss';

const modalRoot = document.getElementById('my_modal');


class PopupModal extends Component
{
    constructor(props)
    {
        super(props);
        this.element = document.createElement('div');
    }
    
    componentDidMount(){
        modalRoot.appendChild(this.element);
    }

    componentWillUnmount(){
        modalRoot.removeChild(this.element);
    }

    render(){
        return(  
            createPortal( this.props.children , this.element )
        )
    }

}



export default PopupModal;