import React, { useState, useContext, useEffect } from 'react';
import { Select } from 'antd';
import axios from 'axios';
import styled from 'styled-components';
import { FilterContext } from '../../Finance_Context/FilterContext';

const { Option } = Select;

const ChangeStatement = () => {

    const { handler_statementId_schema } = useContext(FilterContext);    

    const [statementSource, setStatementSource] = useState([]);
    const [selectboxWidth] = useState('400px');

    useEffect(() => {
        getStatementList();

    }, []);

    const getStatementList = () => {
        axios.post('http://172.28.9.23:5000/api/statmentlist').then(function (response) {
            if (response.data) {
                let statement_option = response.data.statment.map((value, i) => <Option key={value[0]}>{value[2]}</Option>);
                setStatementSource(statement_option);
            }

        }).catch(function (error) {
            console.log('*** Error in getting statement List : ' + error);
        });
    }

    const handleChange = (value) => {

        handler_statementId_schema(value);
    }

    return (
        <StatementNameSelect defaultValue='Select Statement' onChange={handleChange} setWidth={selectboxWidth}>
            {statementSource}
        </StatementNameSelect>
    )
}


let StatementNameSelect = styled(Select)`
    width : ${ props => props.setWidth || '300px'}; 
    outline : none;  
`;

export default ChangeStatement;