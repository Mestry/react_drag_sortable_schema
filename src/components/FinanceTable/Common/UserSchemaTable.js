import React, { useState, useEffect, useContext } from 'react';

import { TableHeader } from './TableHeader';
import { TableRow } from './TableRow';
import { TableContext } from '../Finance_Context/TableContext';

//import CreateSchema from '../../CreateSchema/CreateSchema';

import styled from 'styled-components';

const UserSchemaTable = (props) => {

    const { tableHeaderArray, tableFormulaArray } = useContext(TableContext);

    useEffect(() => {
        console.log(' Ready to load Table...');
        console.log(tableHeaderArray);
        console.log(tableFormulaArray);

    }, [tableHeaderArray, tableFormulaArray])

    return (

        <>
            
            {
                (tableHeaderArray != null && tableHeaderArray != '') && (tableFormulaArray != null && tableFormulaArray != '')
                    ?
                    <TableHandler>
                        <TableWrap className='table_wrap'>
                            <div className='left_list'>
                                <table>
                                    <thead>
                                        <TableHeader list={tableHeaderArray} currencyTxt={'( In Rs. Cr.)'} />
                                    </thead>
                                    <tbody>
                                        {
                                            tableFormulaArray && tableFormulaArray.length > 0 ?
                                                tableFormulaArray.map((item, i) => {
                                                    return <TableRow chartYear={tableHeaderArray} depth={0} key={i} item={item} />
                                                })
                                                :
                                                null
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </TableWrap>
                    </TableHandler>
                    :
                    <div style={{ 'textAlign' : 'center' }}>No data to display...</div>
            }
            
        </>

    )

}


const TableHandler = styled.div`
    //padding : 5px 10px 0px 5px;
    width: 95%;
    margin: 0 auto;
    font-weight: 300;
    font-size: 13px;    
    position: relative;
    border :1px solid #8499a5;  
    border-radius: 6px 0 0 6px;
`;

const TableWrap = styled.div`
    margin-left: 400px;            
    overflow-x: scroll;
    padding-bottom: 15px;
    max-width: 1500px;    
`;


export default UserSchemaTable;

