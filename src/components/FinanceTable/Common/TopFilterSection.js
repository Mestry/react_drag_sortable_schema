import React, { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import ChangeCompany from './ChangeCompany/ChangeCompany';
import ChangeStatement from './SelectStatement/ChangeStatement';
import ChangeType from './ChangeType/ChangeType';

import { FilterContext } from '../Finance_Context/FilterContext'
import { TableContext } from '../Finance_Context/TableContext';

import { MyThemeContext } from '../../../contexts/MyThemeContext';

const TopFilterSection = () => {

    const { companyId_schema, statementId_schema, statementType_schema } = useContext(FilterContext);
    const { getCompanyDetail, tempStmtType, handler_setTempStmtType, tcompanyId,
        tcompanyName } = useContext(TableContext);

    const { theme } = useContext(MyThemeContext);

    const [userId] = useState(6);


    useEffect(() => {
        /* This will set statement type in TableContext context. */
        //console.log('seseeeeeee : '+statementType_schema);
        //handler_setTempStmtType(statementType_schema);
    }, []);


    // console.log("all three values");
    // console.log('companyId_schema ' + companyId_schema);
    // console.log('statementId_schema ' + statementId_schema);
    // console.log('statementType_schema ' + statementType_schema);


    /* ---------------------------------------------- if api is not working : test functionality -------------------- */
    // useEffect(() => {

    //     getCompanyDetail(companyId_schema, statementId_schema, statementType_schema, userId , false);

    // }, []);


    // Custome function out link
    const getCompanyStatementDetail = () => {
        getCompanyDetail(companyId_schema, statementId_schema, statementType_schema, userId, false);

    }

    /* ---------------------------------------------- if api is not working -------------------- */


    useEffect(() => {

        console.log('change2 company id or statement id');
                
        if (companyId_schema != null && statementId_schema != null ) {
            
            getCompanyDetail(companyId_schema, statementId_schema, userId);
        }
        else {
            console.log('Error can not call');
        }

    }, [companyId_schema, statementId_schema ]);


    // This use Effect will call on statement type is changed.
    useEffect(() => {

        //console.log('change2 type: ' + statementType_schema);
        handler_setTempStmtType(statementType_schema);

    }, [statementType_schema]);

    const showNewSchemaForm = () =>{
        console.log('new schema create');
    }


    return (
        <>
            <div> <CompanyH3>{tcompanyName}{tcompanyId} </CompanyH3></div>
            <FilterSectionUl text={theme.text} doubleBg={theme.doubleBg} className='top_filter_container_li'>                
                <li>
                    <ChangeCompany />
                </li>
                <li>
                    <ChangeStatement />
                </li>
                <li>
                    <ChangeType />
                </li>
            </FilterSectionUl>
            <div sttle={{ display : 'flex' }}>
                <span onClick={getCompanyStatementDetail} >&nbsp; Get Dummy Table </span>
                <span onClick={ showNewSchemaForm } >&nbsp; Create New Schema</span>
            </div>
        </>
    )
}

export default TopFilterSection;


let CompanyH3 = styled.h3`
    font-size: 20px;
    text-align :center;
    color : #fff;
    letter-spacing : .4px;
    
`;

let FilterSectionUl = styled.ul`
    className : ${props => props.className};
    display: flex;
    flex-direction: row;
    justify-content: space-between;    
    align-items : center;
    // border :  1px solid ${ props => props.text || '#fff'};
    background : ${ props => props.doubleBg}
    margin : 0 10px; 
    padding : 15px;
    border-radius : 5px;
`;