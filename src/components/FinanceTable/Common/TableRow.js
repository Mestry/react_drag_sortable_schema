import React, { useState, useContext } from 'react';
import styled from 'styled-components';
import { TableContext } from '../Finance_Context/TableContext';
import { Tooltip, Icon, Modal } from 'antd';
import TableChart from '../Chart/TableChart';

export const TableRow = (props) => {

    const { checkColumnPresent, sortColumnAscending, companyName, tableFormulaArray } = useContext(TableContext);

    const [showChild, setshowChild] = useState(false);

    const handleCollapse = () => {
        setshowChild(!showChild);
    }



    const [visible, setvisible] = useState(false);
    const [individualChartDetail, setIndividualChartDetail] = useState([]);

    const getOnlyValue = (dataArray) => {

        let tempArray = [];
        if (dataArray && dataArray.length > 0) {
            dataArray.map((v) => {
                tempArray.push(v.value);
            });
        }
        return tempArray;
    }

    const showModal = (item_data, chartYear) => {
        console.log(item_data);

        let tempObj = {
            'labels_years': chartYear,
            'formula_name': item_data.frml_str,
            'chartData': getOnlyValue(item_data.frml_data),
            'label': companyName,
            'backgroundColor': 'green'
        }

        setIndividualChartDetail(tempObj);

        //setchartData(item_data.frml_data);

        setvisible(true);
    };

    const handleOk = e => {
        //console.log(e);
        setvisible(false);
    };

    const handleCancel = e => {
        //console.log(e);
        setvisible(false);
    };


    const TableLeftField = ({ frml_str, leftPade }) => {


        return (
            frml_str.length > 42 ?
                <Tooltip placement="right" title={frml_str}>
                    {
                        <ValueSpan leftSpace={leftPade}>
                            {frml_str.substr(0, 50) + '...'}
                        </ValueSpan>
                    }
                </Tooltip>
                :
                <ValueSpan leftSpace={leftPade}>{frml_str}</ValueSpan>
        )

    }

    return (
        <>

            <TableUl className='tab_value_col' columnAscending={sortColumnAscending}
                onClick={(props.item.children && props.item.children.length > 0) ? handleCollapse : null}
            >

                <LeftFixLi className='left_title'>
                    <>
                        <TableLeftField frml_str={props.item.frml_str} leftPade={`${props.depth * 10}px`} />
                        {
                            props.item.children && props.item.children.length > 0 ?
                                <span style={{ marginLeft: '10px' }}>
                                    {
                                        !showChild ? '+' : '-'
                                    }
                                </span>
                                :
                                null
                        }
                    </>
                    <div onClick={() => showModal(props.item, props.chartYear)}>
                        <span>
                            <Icon type="bar-chart" style={{ fontSize: '16px', color: '#ffc657' }} />
                        </span>
                    </div>
                </LeftFixLi>

                {
                    (props.item.frml_data && props.item.frml_data.length > 0) && props.item.frml_data.map((numberValue, i) =>
                        <ValueList
                            displayCol={checkColumnPresent(i)}
                            key={i}>
                            {numberValue.value}
                        </ValueList>
                    )
                }
            </TableUl>
            {
                showChild ?
                    <TableInnerRow chartYear={props.chartYear} list={props.item.children} depth={props.depth + 1} />
                    :
                    null
            }

            <Modal
                title={'Comapny Name will come'}
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
                width={1000}
            >
                <TableChart individualChartDetail={individualChartDetail} />
            </Modal>

        </>
    )
}


export const TableInnerRow = (props) => {
    return (
        props.list.map((item, i) =>
            <TableRow key={i} item={item} depth={props.depth} chartYear={props.chartYear} />
        )
    )
}


let ValueSpan = styled.span`
    padding-left : ${ props => props.leftSpace}
`;


const TableUl = styled.tr`
    class : ${ props => props.className};
    display: flex;
    flex-direction: row;
    border-bottom : 1px solid #5d8ea9;
    margin: 0;
    padding: 0;
    flex-direction : ${ props => props.columnAscending === true ? 'row' : 'row-reverse'}
    
     : hover {
        background-color : #3a6277;        
    }
  
    : last-child{
        border-bottom:none;

        & td:first-child{
            border-bottom:none
        }
    }

    & td:hover{
        background-color : #3a6277;
        Xborder-bottom:1px solid #5d8ea9;        
    }
  

`;


const LeftFixLi = styled.td`
    class : ${ props => props.className}    
    padding-left: 10px;   
    padding-right:15px;     
    width: 400px;       
    position: absolute;
    top: auto;
    left: 0;
    text-align: left;
    border-bottom: 1px solid #5d8ea9;    
    cursor: pointer;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items : center;
    line-heigh : 31px;
    //box-shadow:  3px 0px 7px -3px #292929;
    height : 31px;
    
`;

const ValueList = styled.td`        
    display : ${ props => props.displayCol == 'show' ? 'flex' : 'none'}   
    justify-content: flex-end;
    align-items : center;
    min-width : 130px;    
    height: 30px;
`;





