import React, { useContext, useState, useEffect } from 'react';
import { Icon, Popover, Button } from 'antd';

import { TableContext } from '../Finance_Context/TableContext';

import styled from 'styled-components';


export const TableHeader = (props) => {

    const { hiddenColumnList, setHiddenColumnList, checkColumnPresent,
        sortColumnAscending, setSortColumnAscending, tableHeaderArray, showYearCheckBox,
        setShowYearCheckBox } = useContext(TableContext);

    useEffect(() => {
        console.log('after column list change :');
        console.log(hiddenColumnList);
    }, [hiddenColumnList]);

    const toggleCheckBox = (id, newYear) => {

        let filterArray = hiddenColumnList.filter(dd => dd.year == newYear);

        if (filterArray.length > 0) {
            let tempList = [...hiddenColumnList];
            let newList = tempList.filter((v1) => v1.year != newYear);
            setHiddenColumnList(newList);
        }
        else {
            setHiddenColumnList([...hiddenColumnList, { id: id, year: newYear }]);
        }

    }


    const isYearInHiddenList = (year) => {
        let tempList = hiddenColumnList.filter(dd => dd.year == year);
        return tempList.length > 0 ? true : false;
    }

    return (
        <>
            {/* {
                hiddenColumnList.map((v) =>
                    <div>{v.id}:{v.year}</div>
                )
            } */}
            <TableHeadTr className='data_list_header' columnAscending={sortColumnAscending}>
                <LeftPositionTd className='left_title'>
                    <ColumnShowHideUl display_box={showYearCheckBox}>
                        {
                            tableHeaderArray.length > 0 && tableHeaderArray.map((v, i) =>

                                <li
                                    key={i}
                                    onClick={() => toggleCheckBox(i, v.year)}
                                >
                                    <div>
                                        <Icon type={isYearInHiddenList(v.year) ? 'stop' : 'check'} style={{ marginRight: '5px', color: '#fb7d7d' }} />
                                    </div>
                                    <div>{v.year}</div>
                                </li>
                            )
                        }
                    </ColumnShowHideUl>

                    <div>
                        <Icon type="retweet" onClick={() => setSortColumnAscending(!sortColumnAscending)} />
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <Icon type="eye-invisible" onClick={() => { setShowYearCheckBox(!showYearCheckBox) }} />
                    </div>

                    <div>{props.currencyTxt}</div>

                </LeftPositionTd>

                {
                    props.list.map((y, i) =>
                        <ValueList displayCol={checkColumnPresent(i)} key={i}>
                            {y.year}
                        </ValueList>
                    )
                }

            </TableHeadTr>
        </>
    )
}




const TableHeadTr = styled.tr`
    
    display: flex;
    flex-direction: row; 
    background: #0b3044;  
    margin: 0;
    padding: 0;
    flex-direction : ${ props => props.columnAscending === true ? 'row' : 'row-reverse'}
`;


const LeftPositionTd = styled.td`
    class : ${ props => props.className}
    padding-right: 10px;
    padding-left: 10px;     
    width: 400px;       
    position: absolute;
    top: auto;
    left: 0;
    text-align: left;
   // border-bottom: 1px solid #5d8ea9;
    cursor: pointer;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    height : 40px;
    align-items : center;
    border-radius : 5px 0 0 0;
`;

const ValueList = styled.td`    
    display : ${ props =>
        props.displayCol == 'show' ? 'flex' : 'none'
    }   
    justify-content : flex-end;    
    min-width: 130px;       
    height: 40px;
    align-items : center;
    border-bottom : 1px solid #5d8ea9;
`;


let ColumnShowHideUl = styled.ul`

    border : 1px solid #000;        
    background-color : #0d3044;
    max-height : 400px;
    overflow-x : scroll;
    position : absolute;
    left: 0px;
    z-index: 2;
    top: 30px;
    padding : 5px 0px 10px;    
    display : ${ props => props.display_box ? 'block' : 'none'}    

    & > li:last-child{
        border-bottom : none;
    }

    & > li{   
        border-bottom : 1px solid gray  !important;
        display : flex;
        flex-direction : row;                    
        margin : 0 12px 0 0;     
        font-size: 12px;

        & > div {                        
            display : flex;
            flex-direction : row;
            justify-content : space-between;
        }
                
    }

    
`;

