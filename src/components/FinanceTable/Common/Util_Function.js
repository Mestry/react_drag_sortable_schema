
export const getName = () => 'prashant mestry';

const getDatePart = (dateIn) => {
    return dateIn.split(' ')[0];
}

const sortArray = dateArray => {
    return dateArray.sort();

}

/*
* makeTableHeaderArray() function will return array of header
*/
export const makeTableHeaderArray = (stmt_data, date_to_fetch = 'rprt_date') => {

    let temp = [];
    if (stmt_data && stmt_data.length > 0) {

        stmt_data.map((element) => {
            element.frml_data.forEach(val => {
                let dpart = getDatePart(val[date_to_fetch]);

                if (temp.indexOf(dpart) == -1) {
                    temp.push(dpart);
                }              
            });
        });
        
        sortArray(temp);        
    }

    return makePeriodYearArray (temp);    
}



const makePeriodYearArray = (dataArray) =>{

    let tempD = [];
    if(dataArray.length > 0)
    {
        dataArray.map((v) =>{

            tempD.push({ period : v , year : v });
        })
    }
    return tempD;

}




export const makeTable_NestedArray = (companyData, tableHeaderArray) => {

    //console.log('inside function main');
    //console.log(tableHeaderArray);
    //console.log(companyData);

    //
    let getListValue = (tmp_formulaData) => {

        let value_array = [];

        if (tmp_formulaData && tmp_formulaData.length > 0) {
            for (let p = 0; p < tableHeaderArray.length; p++) {
                let flag = false;
                let yearK = tableHeaderArray[p].year;

                for (let m = 0; m < tmp_formulaData.length; m++) {

                    if (yearK == getDatePart(tmp_formulaData[m].rprt_date)) {

                        value_array.push({ 'period': yearK, 'value': tmp_formulaData[m].value });
                        flag = true; break;
                    }
                }

                if (flag == false) {
                    value_array.push({ 'period': yearK, 'value': '-' });
                }
            }
        }
        return value_array;
    }


    let recursiveChild = (loopData) => {
        let temp_obj = [];

        loopData.forEach(element => {
            let tem_value_array = getListValue(element.frml_data);

            let tempA = [];
            if (element.children && element.children.length > 0) {
                tempA.push(recursiveChild(element.children));
            }

            temp_obj.push({ 'frml_id': element.frml_id, 'frml_str': element.frml_str, frml_data: tem_value_array, children: tempA });
        });
        return temp_obj;

    }

    let tempTableData = [];
    let recursiveParent = (loopData) => {

        if (loopData && loopData.length > 0) {
            loopData.forEach(element => {

                let tem_value_array = getListValue(element.frml_data);
                let tempA = [];

                if (element.children && element.children.length > 0) {
                    tempA = recursiveChild(element.children);
                }

                tempTableData.push({ 'frml_id': element.frml_id, 'frml_str': element.frml_str, frml_data: tem_value_array, children: tempA });
            });
        }

    }


    
    if(companyData != null &&  companyData.length > 0)
    {
        recursiveParent(companyData, 0);
    }
    
    return tempTableData;
}


export const createFormulaKeyValeyArray = (formulaArray) => {

    let newKeyValey_formulaArray = [];

    let recursiveData = (data) => {

        if (data.length > 0) {
            data.forEach((v) => {

                newKeyValey_formulaArray.push({ frml_id: v.frml_id, frml_str: v.frml_str, frml_data: v.frml_data });

                if (v.children && v.children.length > 0) {
                    recursiveData(v.children);
                }

            })
        }

    }

    recursiveData(formulaArray);
    return newKeyValey_formulaArray;
}


export const make_MergeTableData = (newKeyValey_formulaArray, userSchema) => {

    let replaceKeyValue = (userSchema) => {

        if (userSchema.length > 0) {
            userSchema.forEach((sValue) => {
                let selectedDataFromKeyValueArray = newKeyValey_formulaArray.find(v1 => sValue.frml_id == v1.frml_id);

                // console.log('value of selectedDataFromKeyValueArray');
                // console.log(selectedDataFromKeyValueArray);

                if (selectedDataFromKeyValueArray != undefined) {
                    sValue.frml_data = selectedDataFromKeyValueArray.frml_data;
                }
                else{
                    console.log('Schema frml_id not found in statement data');
                    sValue.frml_data = [{ period : '' , value : '' }];
                }


                if (sValue.children && sValue.children.length > 0) {
                    replaceKeyValue(sValue.children);
                }
            });
        }

    }

    if (userSchema && userSchema.length > 0) {
        replaceKeyValue(userSchema);
        return userSchema;
    }
    else {
        return [];
    }

}
