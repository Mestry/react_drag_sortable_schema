import React, { useContext, useState } from 'react';
import styled from 'styled-components';

import { FilterContext } from  '../../Finance_Context/FilterContext';
import { MyThemeContext } from '../../../../contexts/MyThemeContext';

const ChangeType = () => {

    const { theme } = useContext(MyThemeContext);
    const { statementArray, statementType_schema, handler_statementType_schema } = useContext(FilterContext);
    const [selectboxWidth] = useState('200px');

    const checkTypeMatch = (stateType, keyType) => {
        return stateType.toLowerCase() == keyType.toLowerCase();
    }

    return (
        <div>
            {
                statementArray.length > 0 && statementArray.map((v, i) =>
                    <StatementTypeToggleButton key={i}  onClick={() => { handler_statementType_schema(v.id) }}
                        active={checkTypeMatch(statementType_schema, v.id)}
                        setWidth={selectboxWidth}
                        hoverColor= '#0a2e42'
                        text={theme.text}
                    >
                        {v.name}
                    </StatementTypeToggleButton>
                )
            }
        </div>
    )
}

const StatementTypeToggleButton = styled.button`
    background: ${props => props.active ? (props.hoverColor) : '#193d4f'};
    color: ${props => props.active ? '#fff' : '#272727'};
    padding : 8px;
    display : inline;
    border : 1px solid #316c8a;
    width : ${ props => props.setWidth || '400px'};
    cursor : pointer;
    font-size: 11px;
    text-transform: uppercase;
    color: ${ props => props.text || '#fff'};
    letter-spacing: .4px;    
    className : ${ props => props.className};
    &:hover { 
        background : ${ props => props.hoverColor || '#043f5f'}                  
    }    
`;


export default ChangeType;