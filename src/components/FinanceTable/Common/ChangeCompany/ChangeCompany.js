import React, { useState, useContext } from 'react';
import { AutoComplete } from 'antd';
import axios from 'axios';
import styled from 'styled-components';


import { FilterContext } from '../../Finance_Context/FilterContext';
const { Option } = AutoComplete;

const ChangeCompany = () => {

    const { is_company_detail_from_api, handler_setCompanyId_schema } = useContext(FilterContext);

    const [dataSourse, setDataSource] = useState([]);
    const [selectboxWidth] = useState('400px');

    function getCompanyList(searchText) {


            axios.post('http://172.28.9.23:5000/api/companylist', {
                searchString: searchText
            }).then(function (response) {
                if (response.data) {
                    let optionList = response.data.records.map((op) => <Option key={op[0]}>{op[1]}</Option>);
                    setDataSource(optionList);
                }
                else {
                    setDataSource([]);
                }

            }).catch(function (error) {
                console.log('*** Error in company list fetching... :' + error);
            });
        
    }

    const onSearch = searchText => getCompanyList(searchText);

    const onSelect = (value) => {
        handler_setCompanyId_schema(value);
    }

    return (
        <CompanySelectAutoComplete
            dataSource={dataSourse}
            onSearch={onSearch}
            onSelect={onSelect}
            placeholder="Enter Company Name"
            setWidth={selectboxWidth}
        />
    )
}

export default ChangeCompany;


let CompanySelectAutoComplete = styled(AutoComplete)`

    width : ${ props => props.setWidth || '300px'};     
    outline : none;    
`;