import React, { useState, useContext, useEffect } from 'react';

import { Line } from 'react-chartjs-2';
import AddCompany from '../AddCompany/AddCompany';

const TableChart = (props) => {

    let default_data =
    {
        labels: [],
        datasets:
            [
                //{   label : 'one1', backgroundColor : '#ffb6aa',    data : chartData    },
                // {   label : 'two',  backgroundColor : 'red',        data : [1,16,1,2,2,14,3]    }
            ]
    }

    const [chartArray, setchartArray] = useState(default_data);

    useEffect(() => {

        console.log('props change in table chart');
        console.log(props);

        let tt = { label: 'two', backgroundColor: '#000', data: [100, 160, 100, 200, 250, 300, 400] }

        let tempData =
        {
            labels: props.individualChartDetail.labels_years.map(v => v.year),
            datasets: [
                {
                    label: props.individualChartDetail.formula_name,    
                    backgroundColor: "#000",
                    data: props.individualChartDetail.chartData,
                    fill: false,
                    borderColor: "red"
                }
                ,
                { ...tt, fill: false, borderColor: "green" }
            ]
        }


        //let newDataset = { ...chartArray , ...tempData };

        console.log('chart props change');
        console.log(tempData);

        setchartArray(tempData);

    }, [props.individualChartDetail]);



    return (
        <div>
            <h2> Formula Name : {(props.individualChartDetail && props.individualChartDetail.formula_name)
                ?
                props.individualChartDetail.formula_name
                : 'not found'}</h2>

<div>Select Company : <AddCompany /></div>
            <Line
                options={{
                    legend: {
                        display: true
                    },
                    responsive: true, hover: {
                        mode: "nearest",
                        intersect: false,
                        axis: 'x'
                    },
                    // tooltips: {
                    //     callbacks: {
                    //         label: function (tooltipItem) {
                    //             console.log(tooltipItem);
                    //             return tooltipItem.yLabel;
                    //         }
                    //     }
                    // },
                    datalabels: {
                        display: false
                    }
                }}
                data={chartArray}
            />


        </div>
    )
}

export default TableChart;