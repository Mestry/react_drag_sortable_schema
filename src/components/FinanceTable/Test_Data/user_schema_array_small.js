export const user_schema_array =
{
    "schema_id": 6,
    "schema_name": "Test Schema One",
    "Xschema": [],
    "schema": [
        {
            "frml_id": "pl_int_sa_074",
            "frml_str": "Reserve 112 schema",
            "frml_data": [],
            "value": "57.33",
            "children": [
                {
                    "frml_id": "pl_int_sa_079",
                    "frml_str": "Adj Calculated EPS (Unit.Curr.)",
                    "frml_data": [],
                    "value": "0",
                    "children": []
                }
            ]
        },
        {
            "frml_id": "pl_int_sa_024",
            "frml_str": "Reserve one",
            "frml_data": [],
            "value": "0",
            "children": []
        },
        {
            "frml_id": "pl_int_con_021",
            "frml_str": "Asset",
            "frml_data": [],
            "value": "0",
            "children": []
        }


    ]
}