export let statement_detail_array =
{
    "comp_id": "132321",
    "comp_name": "Cadila Healthcare Ltd.",
    "data":
        [
            {
                "stmt_id": "1",
                "stmt_name": "Profit & Loss - Interim",
                "stmt_type": "sa",
                "stmt_data": [
                    {
                        "frml_id": "pl_int_sa_074",
                        "frml_str": "Reserve 112 actual",
                        "children": [
                            {
                                "frml_id": "pl_int_sa_024",
                                "frml_str": "Reserve one",
                                "frml_data": [
                                    {
                                        "dscl_date": "2019-11-14 00:00:00",
                                        "rprt_date": "2019-09-30 00:00:00",
                                        "value": "201"
                                    },
                                    {
                                        "dscl_date": "2019-08-14 00:00:00",
                                        "rprt_date": "2019-06-30 00:00:00",
                                        "value": "202"
                                    },
                                    {
                                        "dscl_date": "2019-05-15 00:00:00",
                                        "rprt_date": "2019-03-31 00:00:00",
                                        "value": "203"
                                    },
                                    {
                                        "dscl_date": "2019-02-14 00:00:00",
                                        "rprt_date": "2018-12-31 00:00:00",
                                        "value": "204"
                                    }
                                ]
                            },
                            {
                                "frml_id": "pl_int_sa_0714",
                                "frml_str": "Reserve two",
                                "frml_data": [
                                    {
                                        "dscl_date": "2019-11-14 00:00:00",
                                        "rprt_date": "2019-09-30 00:00:00",
                                        "value": "701"
                                    },
                                    {
                                        "dscl_date": "2019-08-14 00:00:00",
                                        "rprt_date": "2019-06-30 00:00:00",
                                        "value": "702"
                                    },
                                    {
                                        "dscl_date": "2019-05-15 00:00:00",
                                        "rprt_date": "2019-03-31 00:00:00",
                                        "value": "703"
                                    },
                                    {
                                        "dscl_date": "2019-02-14 00:00:00",
                                        "rprt_date": "2018-12-31 00:00:00",
                                        "value": "704"
                                    }
                                ]
                            }
                        ],
                        "frml_data":
                            [
                                {
                                    "dscl_date": "2019-11-14 00:00:00",
                                    "rprt_date": "2019-09-30 00:00:00",
                                    "value": "303"
                                },
                                {
                                    "dscl_date": "2019-08-14 00:00:00",
                                    "rprt_date": "2019-06-30 00:00:00",
                                    "value": "302"
                                },
                                {
                                    "dscl_date": "2019-05-15 00:00:00",
                                    "rprt_date": "2019-03-31 00:00:00",
                                    "value": "301"
                                },
                                {
                                    "dscl_date": "2019-02-14 00:00:00",
                                    "rprt_date": "2018-12-31 00:00:00",
                                    "value": "300"
                                }
                            ]
                    },
                    {
                        "frml_id": "pl_int_sa_079",
                        "frml_str": "Adj Calculated EPS (Unit.Curr.)",
                        "frml_data":
                            [
                                {
                                    "dscl_date": "2019-11-14 00:00:00",
                                    "rprt_date": "2019-09-30 00:00:00",
                                    "value": "1000"
                                },
                                {
                                    "dscl_date": "2019-08-14 00:00:00",
                                    "rprt_date": "2019-06-30 00:00:00",
                                    "value": "900"
                                },
                                {
                                    "dscl_date": "2019-05-15 00:00:00",
                                    "rprt_date": "2019-03-31 00:00:00",
                                    "value": "800"
                                },
                                {
                                    "dscl_date": "2019-02-14 00:00:00",
                                    "rprt_date": "2018-12-31 00:00:00",
                                    "value": "700"
                                }
                            ]
                    }
                ]
            },
            {
                "stmt_id": "1",
                "stmt_name": "Profit & Loss - Interim",
                "stmt_type": "con",
                "stmt_data":
                    [
                        {
                            "frml_id": "pl_int_con_029",
                            "frml_str": "Operating Profit (Excl OI)",
                            "frml_data":
                                [
                                    {
                                        "dscl_date": "2019-11-14 00:00:00",
                                        "rprt_date": "2019-09-30 00:00:00",
                                        "value": "1000"
                                    },
                                    {
                                        "dscl_date": "2019-08-14 00:00:00",
                                        "rprt_date": "2019-06-30 00:00:00",
                                        "value": "900"
                                    },
                                    {
                                        "dscl_date": "2019-05-15 00:00:00",
                                        "rprt_date": "2019-03-31 00:00:00",
                                        "value": "800"
                                    },
                                    {
                                        "dscl_date": "2019-02-14 00:00:00",
                                        "rprt_date": "2018-12-31 00:00:00",
                                        "value": "700"
                                    }
                                ]
                        },
                        {
                            "frml_id": "pl_int_con_082",
                            "frml_str": "Basic EPS before Extraordinary Items",
                            "frml_data":
                                [
                                    {
                                        "dscl_date": "2019-11-14 00:00:00",
                                        "rprt_date": "2019-09-30 00:00:00",
                                        "value": "1000"
                                    },
                                    {
                                        "dscl_date": "2019-08-14 00:00:00",
                                        "rprt_date": "2019-06-30 00:00:00",
                                        "value": "900"
                                    },
                                    {
                                        "dscl_date": "2019-05-15 00:00:00",
                                        "rprt_date": "2019-03-31 00:00:00",
                                        "value": "800"
                                    },
                                    {
                                        "dscl_date": "2019-02-14 00:00:00",
                                        "rprt_date": "2018-12-31 00:00:00",
                                        "value": "700"
                                    }
                                ]
                        },
                        {
                            "frml_id": "pl_int_con_048",
                            "frml_str": "Depreciation",
                            "frml_data":
                                [
                                    {
                                        "dscl_date": "2019-11-14 00:00:00",
                                        "rprt_date": "2019-09-30 00:00:00",
                                        "value": "500"
                                    },
                                    {
                                        "dscl_date": "2019-08-14 00:00:00",
                                        "rprt_date": "2019-06-30 00:00:00",
                                        "value": "501"
                                    },
                                    {
                                        "dscl_date": "2019-05-15 00:00:00",
                                        "rprt_date": "2019-03-31 00:00:00",
                                        "value": "502"
                                    },
                                    {
                                        "dscl_date": "2019-02-14 00:00:00",
                                        "rprt_date": "2018-12-31 00:00:00",
                                        "value": "503"
                                    }
                                ]
                        },
                    ]
            }
        ]
}