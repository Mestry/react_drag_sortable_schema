export const user_schema_array =
{
    "schema_id": 6,
    "schema_name": "Test Schema One",
    "schema": [
        // {
        //     "frml_str": "Expenditure1",
        //     "frml_id": "pl_int_sa_011",
        //     "frml_data": [],
        //     "value": "57.33",
        //     "children": [
        //         {
        //             "frml_str": "(Increase) / Decrease In Stocks",
        //             "frml_id": "pl_int_sa_012",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Cost of Services & Raw Materials",
        //             "frml_id": "pl_int_sa_013",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Purchase of Finished Goods",
        //             "frml_id": "pl_int_sa_014",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Excise Duty",
        //             "frml_id": "pl_int_sa_015",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Operating & Manufacturing Expenses",
        //             "frml_id": "pl_int_sa_016",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Electricity Power & Fuel Cost",
        //             "frml_id": "pl_int_sa_017",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Employee Cost",
        //             "frml_id": "pl_int_sa_018",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "General  Administration Expenses",
        //             "frml_id": "pl_int_sa_019",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Loss on Foreign Exchange",
        //             "frml_id": "pl_int_sa_020",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Loss on Foreign Exchange Loan",
        //             "frml_id": "pl_int_sa_021",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Miscellaneous Expenses",
        //             "frml_id": "pl_int_sa_022",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         }
        //     ]
        // },
        {
            "frml_str": "Interest Expended",
            "frml_id": "pl_int_sa_024",
            "frml_data": [],
            "value": "0",
            "children": []
        },
        {
            "frml_str": "Operating Expenses",
            "frml_id": "pl_int_sa_025",
            "frml_data": [],
            "value": "0",
            "children": []
        },
        // {
        //     "frml_str": "Other Income",
        //     "frml_id": "pl_int_sa_026",
        //     "frml_data": [],
        //     "value": "20.85",
        //     "children": [
        //         {
        //             "frml_str": "Dividend Income",
        //             "frml_id": "pl_int_sa_027",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         },
        //         {
        //             "frml_str": "Export Incentives",
        //             "frml_id": "pl_int_sa_028",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": [
        //                 {
        //                     "frml_str": "General  Administration Expenses",
        //                     "frml_id": "pl_int_sa_019",
        //                     "frml_data": [],
        //                     "value": "0",
        //                     "children": []
        //                 },
        //                 {
        //                     "frml_str": "Loss on Foreign Exchange",
        //                     "frml_id": "pl_int_sa_020",
        //                     "frml_data": [],
        //                     "value": "0",
        //                     "children": [
        //                         {
        //                             "frml_str": "General  Administration Expenses",
        //                             "frml_id": "pl_int_sa_019",
        //                             "frml_data": [],
        //                             "value": "0",
        //                             "children": []
        //                         },
        //                         {
        //                             "frml_str": "Loss on Foreign Exchange",
        //                             "frml_id": "pl_int_sa_020",
        //                             "frml_data": [],
        //                             "value": "0",
        //                             "children": []
        //                         },
        //                         {
        //                             "frml_str": "Loss on Foreign Exchange Loan",
        //                             "frml_id": "pl_int_sa_021",
        //                             "frml_data": [],
        //                             "value": "0",
        //                             "children": []
        //                         },
        //                         {
        //                             "frml_str": "Miscellaneous Expenses",
        //                             "frml_id": "pl_int_sa_022",
        //                             "frml_data": [],
        //                             "value": "0",
        //                             "children": []
        //                         }
        //                     ]
        //                 },
        //                 {
        //                     "frml_str": "Loss on Foreign Exchange Loan",
        //                     "frml_id": "pl_int_sa_021",
        //                     "frml_data": [],
        //                     "value": "0",
        //                     "children": []
        //                 },
        //                 {
        //                     "frml_str": "Miscellaneous Expenses",
        //                     "frml_id": "pl_int_sa_022",
        //                     "frml_data": [],
        //                     "value": "0",
        //                     "children": []
        //                 }
        //             ]
        //         },
        //         {
        //             "frml_str": "Foreign Exchange Gain",
        //             "frml_id": "pl_int_sa_029",
        //             "frml_data": [],
        //             "value": "0",
        //             "children": []
        //         }
        //     ]
        // }
    ]
}