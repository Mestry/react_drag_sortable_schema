
import { user_schema_array } from './user_schema_array';
import { statement_detail_array } from './statement_array';


// user request company detail on companyid , statementid , userid 
// if user has schema relacted to statementid then schema detail will have data.
// otherwise company

export const company_detail_statement = {

    "user_id": 'prashant_100',
    "company_detail": statement_detail_array,
    "schema_detail": user_schema_array,
    //"schema_detail" : [],
}

// For dummy schema
export const user_schema_con =
{
    "type": 'con',
    "data":
    {
        "schema_id": 6,
        "schema_name": "User Schema On Consolidated",
        "schema": [
            {
                "frml_id": "pl_int_con_025",
                "frml_str": "Interest Expended : schema con",
                "frml_data": [],
                "value": "",
                "children": [
                    {
                        "frml_id": "pl_int_con_001",
                        "frml_str": "Date End : schema con",
                        "frml_data": [],
                        "value": "0",
                        "children": []
                    },
                    {
                        "frml_id": "pl_int_con_058",
                        "frml_str": "Extraordinary Items : schema con",
                        "frml_data": [],
                        "value": "0",
                        "children": []
                    }                    
                ]
            },
            {
                "frml_id": "pl_int_con_081",
                "frml_str": "Date End : schema con",
                "frml_data": [],
                "value": "",
            },
            {
                "frml_id": "pl_int_con_068",
                "frml_str": "Other Adjustments : schema con",
                "frml_data": [],
                "value": "",
            }
        ]
    }

}


export const user_schema_sa =
{
    "type": 'sa',
    "data":
    {
        "schema_id": 6,
        "schema_name": "User Schema On Standalone",
        //"schema" : [],
        "schema": [
            {
                "frml_str": "Profit On Sale Of Assets : schema sa Profit On Sale Of AssetsProfit On Sale Of Assets",
                "frml_id": "pl_int_sa_037",
                "frml_data": [],
                "value": "",
                // "children": [
                //     {
                //         "frml_id": "pl_int_sa_007",
                //         "frml_str": "Interest on Balances With RBI Other Inter Bank Funds : schema sa",
                //         "frml_data": [],
                //         "value": "0",
                //         "children": []
                //     }
                // ]
            },
            {
                "frml_id": "pl_int_sa_078",
                "frml_str": "Calculated EPS Annualised (Unit.Curr.) : schema sa ",
                "frml_data": [],
                "value": "",
            },
            {
                "frml_id": "pl_int_sa_046",
                "frml_str": "Depreciation : schema sa",
                "frml_data": [],
                "value": "",
            },
            // {
            //     "frml_id": "pl_int_sa_007",
            //     "frml_str": "Interest on Balances With RBI Other Inter Bank Funds : schema",
            //     "frml_data": [],
            //     "value": "0",
            //     "children": []
            // }            
        ]
    }
}