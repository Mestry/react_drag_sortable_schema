import React, { useState, useContext } from 'react';
import { AutoComplete } from 'antd';
import axios from 'axios';
import styled from 'styled-components';

const { Option } = AutoComplete;

const AddCompany = () => {

    //const { handler_setCompanyId_schema } = useContext(FilterContext);

    const [dataSourse, setDataSource] = useState([]);
    const [selectboxWidth] = useState('400px');

    function getCompanyList(searchText) {

            axios.post('http://172.28.9.23:5000/api/companylist', {
                searchString: searchText
            }).then(function (response) {
                if (response.data) {
                    let optionList = response.data.records.map((op) => <Option key={op[0]}>{op[1]}</Option>);
                    setDataSource(optionList);
                }
                else {
                    setDataSource([]);
                }

            }).catch(function (error) {
                console.log('*** Error in company list fetching... :' + error);
            });
        
    }

    //const onSearch = searchText => getCompanyList(searchText);


    const getDummyCompanyList = () =>{

    let f =  [<Option key={100}>100</Option>,<Option key={200}>200</Option>,<Option key={300}>300</Option>];
        setDataSource(f);
    }
    const onSearch = searchText => getDummyCompanyList(searchText);

    const onSelect = (value) => {
        //handler_setCompanyId_schema(value);

        console.log('call api to get formula related data');
    }

    return (
        <CompanySelectAutoComplete
            dataSource={dataSourse}
            onSearch={onSearch}
            onSelect={onSelect}
            placeholder="Enter Company Name"
            setWidth={selectboxWidth}
        />
    )
}

export default AddCompany;


let CompanySelectAutoComplete = styled(AutoComplete)`

    width : ${ props => props.setWidth || '300px'};     
    outline : none;    
`;

