import React from 'react'

import TopFilterSection from './Common/TopFilterSection';
import UserSchemaTable from './Common/UserSchemaTable';

import { TableContextProvider } from './Finance_Context/TableContext';
import { FilterContextProvider } from './Finance_Context/FilterContext';

import './FinanceTable.scss';

const FinanceTableGroup = () => {

    return (
        <div>
            <TableContextProvider>

                <div style={{ width: '95%' , margin : '0 auto' }}>
                    <FilterContextProvider >
                        <TopFilterSection />
                    </FilterContextProvider>

                    <UserSchemaTable />
                </div>

            </TableContextProvider>
        </div>
    )

}

export default FinanceTableGroup