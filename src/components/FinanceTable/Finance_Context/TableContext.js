import React, { useState, createContext, useEffect } from 'react';
import axios from 'axios';
import { makeTableHeaderArray, makeTable_NestedArray, make_MergeTableData, createFormulaKeyValeyArray } from '../Common/Util_Function';

//import { company_detail } from '../Array_Dummy/schema_default_array';



import { company_detail_statement, user_schema_sa, user_schema_con } from '../Test_Data/company_api_array';

export const TableContext = createContext();

export const TableContextProvider = (props) => {

    const [tcompanyId, setTcompanyId] = useState('');
    const [tcompanyName, setTcompanyName] = useState('');
    const [companyDetail, setCompanyDetail] = useState(null);

    const [statementData, setStatementData] = useState(null);

    const [statementTypeData, setStatementTypeData] = useState(null);

    const [globalSchema, setGlobalSchema] = useState(null);
    const [userSchemaDetail, setUserSchemaDetail] = useState(null);

    const [tempStmtType, setTempStmtType] = useState(null);


    const [tableHeaderArray, setTableHeaderArray] = useState(null);
    const [tableFormulaArray, setTableFormulaArray] = useState(null);

    const [showYearCheckBox, setShowYearCheckBox] = useState(false);

    const [hiddenColumnList, setHiddenColumnList] = useState([]);  // default is : []  , //{id : 1 , year : '2001-03'},{id : 2 , year : '2002-03'}
    const [sortColumnAscending, setSortColumnAscending] = useState(true);


    const [displayModule, setDisplayModule] = useState([]);

    const checkColumnPresent = (index) => {

        let tempD = hiddenColumnList.filter((v) => v.id == index);
        return tempD.length == 0 ? 'show' : 'hide';

    }


    const handler_setTempStmtType = (type) => {

        setTempStmtType(type);        
        
    }

    useEffect(() => {
        console.log('temp stmt type set ' + tempStmtType);

        setting_Specific_statementTypeData();
        setting_Temporary_Schema();

    }, [tempStmtType]);


    useEffect(() => {

        console.log('specific statement Data set done : set table_header_array data');

        setShowYearCheckBox(false);


        console.log('statementTypeData ::: ');
        console.log(statementTypeData);        

        let table_header_array = null;
        let formulaArray = null;

        if (statementTypeData != null ) {
            table_header_array = statementTypeData ? makeTableHeaderArray(statementTypeData.stmt_data) : null;

            formulaArray = makeTable_NestedArray(statementTypeData.stmt_data, table_header_array);
        }
        else {
            console.log('statement data not found');
        }


        console.log('both data table');
        console.log(table_header_array);
        console.log(formulaArray);
        console.log('global and user schema below');
        console.log(globalSchema);
        console.log(userSchemaDetail);

        if (table_header_array != null) {

            if (userSchemaDetail != null && userSchemaDetail.length > 0) {

                let newKeyValey_formulaArray = [];
                newKeyValey_formulaArray = createFormulaKeyValeyArray(formulaArray);

                console.log('key value array');
                console.log(newKeyValey_formulaArray);

                let merge_data = [];
                merge_data = make_MergeTableData(newKeyValey_formulaArray, userSchemaDetail);
                console.log(merge_data);

                setTableHeaderArray(table_header_array);
                setTableFormulaArray(merge_data);

            }
            else {
                console.log('schema data not created');
                setTableHeaderArray(table_header_array);
                setTableFormulaArray(formulaArray);
            }

        }
        else {
            console.log('table header array not created');
            setTableHeaderArray(null);
            setTableFormulaArray(null);
        }

    }, [statementTypeData]);






    /* This function will call 2 times: 1st when first time company id and statement id get changed. 2nd when
    *   Statement type is change so that new statement type is set.
    */
    const setting_Specific_statementTypeData = () => {

        console.log(' setting specific statement data');
        console.log(statementData);

        let specific_statementtype_data = [];

        if (tempStmtType != '' && statementData != null && statementData.length > 0) {
            specific_statementtype_data = statementData.find((arrayV) => {
                return arrayV.stmt_type == tempStmtType
            });
        }

        console.log('statement Data');
        console.log(tempStmtType);
        console.log('specifice statement type Data');
        console.log(specific_statementtype_data);

        if (specific_statementtype_data != undefined) {

            setHiddenColumnList([]);
            setStatementTypeData(specific_statementtype_data);
        }
        else {
            console.log('statement type data is not found');
            setStatementTypeData(null);
        }

    }


    useEffect(() => {
        console.log(' statement data is set now set statement type data');

        setting_Specific_statementTypeData();
        

    }, [statementData]);


    useEffect(() => {
        console.log(' companyDetail set ');
        console.log('now set statement data');
        console.log(companyDetail);

        if (companyDetail != null) {
            setStatementData(companyDetail.data);
        }
        else {
            setStatementData(null);
        }

        setting_Temporary_Schema();

    }, [companyDetail]);




    const setting_Temporary_Schema = () => {

        console.log('inside setting temp schema :' + tempStmtType);

        if (tempStmtType == 'con') {
            console.log('inside con schme');

            setUserSchemaDetail(null);
        }
        if (tempStmtType == 'sa') {
            console.log('inside sa schme : global schema is');
            console.log(globalSchema);

            if (globalSchema != null) {

                console.log('set schema for sa');
                setUserSchemaDetail(globalSchema.schema);
            }            
            else{
                setUserSchemaDetail(null);
            }
            
        }

    }



    useEffect(() => {
        console.log('userSchemaDetail set ');
        console.log(userSchemaDetail);

    }, [userSchemaDetail]);



    //
    const getCompanyDetail = (cmp_id, stmt_id, user_id) => {

        // Set statement type in this context file for future reference.        

        console.log("company detail call");
        console.log(cmp_id, " : ", stmt_id, " : ", user_id);


        let api_call = true;

        if (api_call && cmp_id != null && stmt_id != null) {

            let schema_id = 6;
            //let companyDetailUrl = `http://172.28.9.23:5002/api/company/${cmp_id}/statement/${stmt_id}/type/${stmt_type}`;
            let companyDetailUrl = `http://172.28.9.23:5001/api/company/${cmp_id}/statement/${stmt_id}/schema/${schema_id}`;

            console.log('inside api call : api');
            console.log(companyDetailUrl);

            axios.get(companyDetailUrl)
                .then(function (response) {

                    console.log("api response api");
                    console.log(response);

                    if (response && response.data) {

                        console.log('Api res : ===========');
                        console.log(response.data);

                        // Below schema is set manually since schema is not getting properly from API.                        

                        setGlobalSchema(response.data.schema_detail);                        

                        setCompanyDetail(response.data.company_detail);

                        setTcompanyId(response.data.company_detail.comp_id);
                        setTcompanyName(response.data.company_detail.comp_name);

                    }
                    else {
                        console.log('Api bot giving res : XXXXXXXXXXXXX');
                        setCompanyDetail(null);
                        setUserSchemaDetail(null);
                    }

                }).catch(function (error) {
                    console.log('*** Error in Company api call... : ' + error);
                    setCompanyDetail(null);
                });

        }
        else {
            console.log('company data without using api : ' + tempStmtType);

            // For temporary setting user schema : because schema is not getting from api.
            //setting_Temporary_Schema();

            setCompanyDetail(company_detail_statement.company_detail);

            setTcompanyId(company_detail_statement.company_detail.comp_id);
            setTcompanyName(company_detail_statement.company_detail.comp_name);

        }
    }

    return (
        <TableContext.Provider value={{

            //get_dummy_data: get_dummy_data,

            companyDetail: companyDetail,
            tcompanyId: tcompanyId,
            tcompanyName: tcompanyName,
            getCompanyDetail: getCompanyDetail,

            tempStmtType: tempStmtType,
            handler_setTempStmtType: handler_setTempStmtType,

            //filter_companydetail_on_type_handler: filter_companydetail_on_type_handler,

            tableHeaderArray: tableHeaderArray,
            tableFormulaArray: tableFormulaArray,

            setting_Specific_statementTypeData: setting_Specific_statementTypeData,

            sortColumnAscending: sortColumnAscending,
            setSortColumnAscending: setSortColumnAscending,
            checkColumnPresent: checkColumnPresent,

            hiddenColumnList: hiddenColumnList,
            setHiddenColumnList: setHiddenColumnList,

            showYearCheckBox: showYearCheckBox,
            setShowYearCheckBox: setShowYearCheckBox

        }}>
            {props.children}
        </TableContext.Provider>
    )

}