import React, { useState, createContext, useEffect } from 'react';
import axios from 'axios';
import { makeTableHeaderArray, makeTable_NestedArray, make_MergeTableData, createFormulaKeyValeyArray } from '../Common/Util_Function';

//import { company_detail } from '../Array_Dummy/schema_default_array';



import { company_detail_statement, user_schema_sa, user_schema_con } from '../Test_Data/company_api_array';

export const TableContext = createContext();

export const TableContextProvider = (props) => {

    const [tcompanyId, setTcompanyId] = useState('');
    const [tcompanyName, setTcompanyName] = useState('');
    const [companyDetail, setCompanyDetail] = useState(null);

    const [ statementData , setStatementData ] = useState(null);

    const [ statementTypeData , setStatementTypeData] = useState([]);
    const [userSchemaDetail, setUserSchemaDetail] = useState(null);

    const [tempStmtType, setTempStmtType] = useState(null);

    
    const [tableHeaderArray, setTableHeaderArray] = useState([]);
    const [tableFormulaArray, setTableFormulaArray] = useState([]);


    const [hiddenColumnList, setHiddenColumnList] = useState([]);  // default is : []  , //{id : 1 , year : '2001-03'},{id : 2 , year : '2002-03'}
    const [sortColumnAscending, setSortColumnAscending] = useState(true);

    const checkColumnPresent = (index) => {

        let tempD = hiddenColumnList.filter((v) => v.id == index);
        return tempD.length == 0 ? 'show' : 'hide';

    }


    const handler_setTempStmtType = (type) =>{
        
        setTempStmtType(type);        
    }

    useEffect(()=>{        
        setting_Temporary_Schema();
        setting_Specific_statementData_using_userSchema_type();        

    },[tempStmtType]);



    /* It will set specific statement(con|sa) data in state. */

    // const filter_companydetail_on_type_handler = () => {

    //     console.log('call filter_companydetail_on_type_handler for type: '+tempStmtType);
    //     console.log(companyDetail);


    //     setTemporary_Schema();

    //     let specific_statementtype_data = [];
        
    //     if (tempStmtType != '' && companyDetail != null && companyDetail.data.length > 0) {
    //         specific_statementtype_data = companyDetail.data.find((arrayV) => {

    //             return arrayV.stmt_type == tempStmtType
    //         });
    //     }

    //     console.log('specific stmt data');
    //     console.log(specific_statementtype_data);
        
    //     setStatementTypeData(specific_statementtype_data);
    // }


    useEffect(() => {
        console.log('Table Header Array created');
        console.log(tableHeaderArray);

        let formulaArray = [];

        formulaArray = makeTable_NestedArray( statementTypeData.stmt_data, tableHeaderArray);

        // console.log('formula array : *');
        // console.log(formulaArray);

        let newKeyValey_formulaArray = createFormulaKeyValeyArray(formulaArray);

        console.log('Key Value formulaArray : *');
        console.log(newKeyValey_formulaArray);

        console.log('userSchemaDetail array');
        console.log(userSchemaDetail);

        //return;

        let merge_data = {};
        let userSchema_data = userSchemaDetail && userSchemaDetail.data.schema.length > 0 ? userSchemaDetail.data.schema : [];

        console.log(userSchema_data);

        merge_data = make_MergeTableData(newKeyValey_formulaArray, userSchema_data);

        console.log('merge data array');
        console.log(merge_data);

        setTableFormulaArray(merge_data);

        // if (merge_data && merge_data.length > 0) {
        //     /* If user schema present : set schema merge data */
        //     console.log('first cond : schema match : showing schema data');
        //     setTableFormulaArray(merge_data);
        // }
        // else {
        //     /* If user schema not present : set default statement data */
        //     console.log('second cond : schema not match : showing default data');
        //     setTableFormulaArray(formulaArray);

        // }

    }, [tableHeaderArray]);




    useEffect(() => {
        console.log('specific statement Data set done : set table_header_array data');        

        let table_header_array = null;
            table_header_array = statementTypeData  ? makeTableHeaderArray( statementTypeData.stmt_data) : null;   
            
            console.log(table_header_array);

            setTableHeaderArray(table_header_array);

            console.log('here after table header array set');

    }, [statementTypeData]);



    /* This function will call 2 times: 1st when first time company id and statement id get changed. 2nd when
    *   Statement type is change so that new statement type is set.
    */
    const setting_Specific_statementData_using_userSchema_type = () => {

        console.log(' setting specific statement data');    

        let specific_statementtype_data = [];

        if (tempStmtType != '' && statementData != null && statementData.length > 0) {
            specific_statementtype_data = statementData.find((arrayV) => {
                return arrayV.stmt_type == tempStmtType
            });
        }

        console.log(specific_statementtype_data);

        setStatementTypeData(specific_statementtype_data);
    }


    useEffect(() => {
        console.log(' statement data is set now set statement type data');    

        setting_Specific_statementData_using_userSchema_type();
       
    }, [statementData]);


    useEffect(() => {
        console.log(' companyDetail set ');
        console.log('now set statement data');
        console.log(companyDetail);

        if(companyDetail != null)
        {
            setStatementData(companyDetail.data);
        }
        else{
            setStatementData(null);
        }

    }, [companyDetail]);




    const setting_Temporary_Schema  = () => {

        console.log('inside setting temp schema :' +tempStmtType);
        
        if (tempStmtType == 'con') {
            console.log('set schema of con');
            setUserSchemaDetail(user_schema_con);
        }
        if (tempStmtType == 'sa') {
            console.log('set schema of sa');
            setUserSchemaDetail(user_schema_sa);
        }
    }


    useEffect(() => {
        console.log('userSchemaDetail set ');
        console.log(userSchemaDetail);

    }, [userSchemaDetail]);


    
    //
    const getCompanyDetail = (cmp_id, stmt_id, user_id ) => {

        // Set statement type in this context file for future reference.        

        let api_call = false;

        if (api_call && cmp_id != null && stmt_id != null) 
        {

            let schema_id = 6;
            //let companyDetailUrl = `http://172.28.9.23:5002/api/company/${cmp_id}/statement/${stmt_id}/type/${stmt_type}`;
            let companyDetailUrl = `http://172.28.9.23:5001/api/company/${cmp_id}/statement/${stmt_id}/type/${tempStmtType}/schema/${schema_id}`;

            //console.log('inside api call : api');
            console.log(companyDetailUrl);

            axios.get(companyDetailUrl)
                .then(function (response) {

                    if (response && response.data) {

                        //console.log('===========');
                        //console.log(response.data);

                        // Below schema is set manually since schema is not getting properly from API.
                        setting_Temporary_Schema();
                        
                        setCompanyDetail(response.data.company_detail);

                        setTcompanyId(response.data.company_detail.comp_id);
                        setTcompanyName(response.data.company_detail.comp_name);


                    }
                    else {
                        setCompanyDetail(null);
                    }

                }).catch(function (error) {
                    console.log('*** Error in Company api call... : ' + error);

                });

        }
        else {
            console.log('company data without using api : ' + tempStmtType);

            // For temporary setting user schema : because schema is not getting from api.
            setting_Temporary_Schema();           
            
            setCompanyDetail(company_detail_statement.company_detail);
            
            setTcompanyId(company_detail_statement.company_detail.comp_id);
            setTcompanyName(company_detail_statement.company_detail.comp_name);

           
        }
    }

    return (
        <TableContext.Provider value={{

            //get_dummy_data: get_dummy_data,

            companyDetail: companyDetail,
            tcompanyId: tcompanyId,
            tcompanyName: tcompanyName,
            getCompanyDetail: getCompanyDetail,

            tempStmtType    :   tempStmtType,
            handler_setTempStmtType : handler_setTempStmtType,

            //filter_companydetail_on_type_handler: filter_companydetail_on_type_handler,

            tableHeaderArray: tableHeaderArray,
            tableFormulaArray: tableFormulaArray,

            setting_Specific_statementData_using_userSchema_type : setting_Specific_statementData_using_userSchema_type,

            sortColumnAscending: sortColumnAscending,
            setSortColumnAscending: setSortColumnAscending,
            checkColumnPresent: checkColumnPresent,

            hiddenColumnList: hiddenColumnList,
            setHiddenColumnList: setHiddenColumnList,

        }}>
            {props.children}
        </TableContext.Provider>
    )

}