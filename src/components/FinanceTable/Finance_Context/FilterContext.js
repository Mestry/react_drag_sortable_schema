import React, { createContext, useState, useEffect } from 'react';

export const FilterContext = createContext([]);


/*
*   'CompanyFilterContextProvider' will share below object :
*   company id | statement id | statement type and al handlers to change these object values.
*/

export const FilterContextProvider = (props) => {

    const [companyId_schema, setCompanyId_schema] = useState(null);             // default : null
    const [statementId_schema, setStatementId_schema] = useState(null);             // default : null
    const [statementType_schema, setStatementType_schema] = useState('sa');            // default : null

    const [statementArray] = useState([
        { id: 'sa', name: 'Standalone' },
        { id: 'con', name: 'Consolidated' }
    ]);

    // useEffect(() => {
    //     console.log('change in any ::');
    //     console.log(companyId_schema + ' : ' + statementId_schema + ' : ' + statementType_schema);

    // }, [companyId_schema, statementId_schema, statementType_schema]);

    const handler_setCompanyId_schema = (compid) => {
        if (compid) {
            setCompanyId_schema(compid);
        }
    }

    const handler_statementId_schema = (stmt_id) => {
        if (stmt_id) {
            setStatementId_schema(stmt_id);
        }
    }

    const handler_statementType_schema = (stmt_type) => {
        if (stmt_type) {
            setStatementType_schema(stmt_type);
        }
    }


    return (
        <FilterContext.Provider value={{

            companyId_schema: companyId_schema,
            statementId_schema: statementId_schema,
            statementType_schema: statementType_schema,

            statementArray: statementArray,

            handler_setCompanyId_schema: handler_setCompanyId_schema,
            handler_statementId_schema: handler_statementId_schema,
            handler_statementType_schema: handler_statementType_schema,
            
        }}>
            {props.children}
        </FilterContext.Provider>
    )

}
