import React, { useContext } from 'react'
import styled from 'styled-components'
import dsp_logo from '../images/dsp_logo_final.png'
import dsp_logo_light from '../images/dsp_light_logo.png'
import { NavLink } from 'react-router-dom'
import Icon from 'antd/es/icon';
import Tooltip from 'antd/es/tooltip';
import { ThemeContext } from 'styled-components'
import { MyThemeContext } from '../contexts/MyThemeContext'
import ToggleTheme from './ToggleTheme'

const Header = () => {
    const { theme } = useContext(ThemeContext)
    const { currentTheme } = useContext(MyThemeContext)
    console.log(currentTheme.currentTheme)
    const headerStyle = {
        icon: {
            fontSize: "2em",
            color: theme.text
        },
        activeNavLink: {
            backgroundColor: theme.bgDark
        },
        navLink: {
            padding: "0.5em",
            margin: "0 0.5em 0 0"
        }
    }


    return (
        <MyHeader>
            <DSPLogo src={currentTheme.currentTheme === "dark" ? dsp_logo : dsp_logo_light} />
            <MenuIcons>
                <ToggleTheme />
                <NavLink exact style={headerStyle.navLink} activeStyle={headerStyle.activeNavLink} to="/">
                    <Tooltip placement="bottom" title={"home"}>
                        <Icon style={headerStyle.icon} type="home" />
                    </Tooltip>
                </NavLink>
                <NavLink exact style={headerStyle.navLink} activeStyle={headerStyle.activeNavLink} to="/backtest">
                    <Tooltip placement="bottom" title={"backtest"}>
                        <Icon style={headerStyle.icon} type="fund" />
                    </Tooltip>
                </NavLink>
                <NavLink exact style={headerStyle.navLink} activeStyle={headerStyle.activeNavLink} to="/strategy">
                    <Tooltip placement="bottom" title={"strategy"}>
                        <Icon style={headerStyle.icon} type="dollar" />
                    </Tooltip>
                </NavLink>
                <NavLink exact style={headerStyle.navLink} activeStyle={headerStyle.activeNavLink} to="/settings">
                    <Tooltip placement="bottom" title={"settings"}>
                        <Icon style={headerStyle.icon} type="setting" />
                    </Tooltip>
                </NavLink>
                
                <NavLink exact style={headerStyle.navLink} activeStyle={headerStyle.activeNavLink} to="/finance-table">
                    <Tooltip placement="bottom" title={"Finance Table"}>                            
                            <Icon style={headerStyle.icon} type="deployment-unit" />
                    </Tooltip>
                </NavLink>

                <NavLink exact style={headerStyle.navLink} activeStyle={headerStyle.activeNavLink} to="/create-schema">
                    <Tooltip placement="bottom" title={"Create Schema"}>                            
                            <Icon style={headerStyle.icon} type="deployment-unit" />
                    </Tooltip>
                </NavLink>


            </MenuIcons>
        </MyHeader>
    )
}

const MyHeader = styled.div`
    width : 100%;
    display : flex;
    align-items : center;
    justify-content : space-between;
`

const DSPLogo = styled.img`
    height : 40px;
`

const MenuIcons = styled.div`
    display : flex;
    align-items : center
`


export default Header