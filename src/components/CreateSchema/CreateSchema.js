import React, { Component, useState, useEffect } from 'react';

import 'react-sortable-tree/style.css';

import NewSchemaTest2 from '../CreateSchema/NewSchemaTest2';

import { DrageContextProvider, DragContext } from './DragContext';
import './CreateSchema.scss';

const CreateSchema = () => {

    return (
        <>
            <h2>User Schema Creation</h2>

            <div style={{ display: 'flex', color: '#000' }}>

                <DrageContextProvider>
                    <NewSchemaTest2 />
                </DrageContextProvider>

            </div>

        </>
    )
}

export default CreateSchema;

