import React, { useState } from 'react';
import { Item } from 'rc-menu';

import  NewSchemaTest1  from './NewSchemaTest1';



const RootElement = ({ i }) => {

    let selectId = `drop_${i}`;
    let inputId = `input_${i}`;

    return (

        <ul className='my_field'>
            <li>
                <select id={selectId}>
                    <option value='1'>One</option>
                    <option value='2'>Two</option>
                    <option value='3'>Three</option>
                    <option value='4'>Four</option>
                    <option value='5'>Five</option>
                </select>
            </li>
            <li>
                <input type="text" id={inputId} />
            </li>
        </ul>
    )
}



const NewElement = () => {
    return (

        <ul className='my_field'>
            <li>
                <select>
                    <option value='1'>One</option>
                    <option value='2'>Two</option>
                    <option value='3'>Three</option>
                    <option value='4'>Four</option>
                    <option value='5'>Five</option>
                </select>
            </li>
            <li>
                <input type="text" />
                <button>Add</button>
            </li>
        </ul>
    )
}


const MakeSchema = () => {

    const blankCat = { name :'' , age : '' };
    const [catState, setCatState] = useState([{...blankCat}]);

    // const [counter, setCounter] = useState(1);
    // const [item, setItem] = useState([]);


    // const [rootFormula, setRootFormula] = useState([1]);
    // const [childFormula, setchildFormula] = useState([]);


    // const getElement = () => {

    //     //setItem([...item, cc]);
    //     setCounter(counter + 1);
    // }

    const addCat = () => {

        setCatState([...catState , { ...blankCat }])
    }

    return (
        <div>

            <br/><br/><br/><br/>

            <input type="button" value="Add new cat" onClick={addCat} />
            {
                catState.map((v, idx) => {

                    const catId = `name_${idx}`;
                    const ageId = `age_${idx}`;

                    return (
                        <div key={`cat_${idx}`}>
                            <input type="text"
                                name={catId}
                                data-idx={idx}
                                id={catId}
                                className="name"
                                placeholder="formula"
                            />
                            <input
                                type="text"
                                name={ageId}
                                data-idx={idx}
                                id={ageId}
                                className="age"
                                placeholder="age"
                            />
                        </div>
                    )

                })
            }
            <input type="submit" value="submit" />





            {/* 

            <ul className='root_ul'>
                <li>
                    {
                        rootFormula.map((v , i) =>
                            <RootElement id={i}/>
                        )
                    }
                    
                </li>

                <li>
                    <span style={{ float: 'right' }}>
                        <button onClick={ addRoot }>Add Root</button>
                    </span>
                </li>
            </ul>


            <div style={{ border: '1px solid gray', width: '300px', margin: '20px', minHeight: '20px', padding: '10px' }}>
                {
                    item.map((v, i) => v)
                }
            </div>

            <button onClick={getElement}>Add Element</button> */}
        </div>
    )
}

export default MakeSchema;