import React, { useState, useContext, useRef } from 'react';
import SortableTree from 'react-sortable-tree';
import { getNodeAtPath, addNodeUnderParent, walkDescendants, map, getDescendantCount, removeNodeAtPath, changeNodeAtPath } from "react-sortable-tree";
import FileExplorerTheme from 'react-sortable-tree-theme-full-node-drag';

import { DragContext } from './DragContext';

const NewSchemaTest2 = () => {

  const { rawArray, setRawArray } = useContext(DragContext);

  const [tempTxt, setTempTxt] = useState('');

  const getNodeKey = ({ treeIndex }) => treeIndex;

  const onSubmitForm = (node, path, event) => {

    console.log('form submit');
    event.preventDefault();
    const { needsTitle, ...nodeWithoutNeedsTitle } = node;

    console.log('needsTitle');
    console.log(nodeWithoutNeedsTitle);

    console.log('state txt ' + tempTxt);

    nodeWithoutNeedsTitle.title = tempTxt;

    if (nodeWithoutNeedsTitle.title.length > 0) {
      setRawArray(changeNodeAtPath({
        treeData: rawArray,
        path,
        getNodeKey,
        newNode: nodeWithoutNeedsTitle,
      }))

      setTempTxt('');
    }
    else {
      console.log('no title found');
      setTempTxt('');
    }

  }

  const onInputChange = (node, path, event) => {
    const title = event.target.value;


    setTempTxt(title);

    // setRawArray(changeNodeAtPath({
    //   treeData: rawArray,
    //   path,
    //   getNodeKey,
    //   newNode: { ...node, title },
    // }),
    // );

  }

  const onAddNode = (node, path, treeIndex, e) => {


    console.log('====== add node at path ' + path);
    console.log('treeIndex ' + treeIndex);
    console.log('add node at node is ' + node.title);

    //let dd = getDescendantCount({node});
    //console.log('descendant count '+dd);

    let foundNode = getNodeAtPath({ treeData: rawArray, path, getNodeKey, ignoreCollapsed: true });
    console.log('found node');
    console.log(foundNode);


    let addNodeFlag = true;

    if (foundNode.node && foundNode.node.title == '') {
      addNodeFlag = false;
    }
    else if (foundNode.node && foundNode.node.title != '' && foundNode.node.children && foundNode.node.children.length > 0) {
      let isTitleEmpty = (nodeArray) => {
        for (let i = 0; i < nodeArray.length; i++) {
          if (nodeArray[i].title == '') {
            addNodeFlag = false;
            break;
          }
          else if (nodeArray[i].children && nodeArray[i].children.length > 0) {
            isTitleEmpty(nodeArray[i].children);
          }
        }
      }
      isTitleEmpty(foundNode.node.children);
    }




    if (node.title != '' && addNodeFlag) {
      console.log('add new node');
      setRawArray(addNodeUnderParent({
        treeData: rawArray,
        parentKey: path[path.length - 1],
        expandParent: true,
        getNodeKey,
        newNode: {
          title: '',
          needsTitle: true,
        },
      }).treeData,
      )
    }
    else {
      console.log('title is empty: please provide title');
    }


  }


  const removeNode = (node, path, e) => {
    console.log('remove');

    //const deleteFlag = window.confirm('Are you confirm to delete this node?');

    if (true) {
      setRawArray(removeNodeAtPath({
        treeData: rawArray,
        path,
        getNodeKey,
      }))
    }
    else {
      console.log('you said no to delete node');
    }

  }


  const onTreeChange = (treeData) => {
    console.log('on change tree');
    setRawArray(treeData);
    console.log(treeData);
  }

  const onSelectChange = (e) => {
    console.log('select change');
    console.log(e.target.value);

    //setRawArray();

  }


  return (
    <div style={{ height: 400, width: 500, border: '1px solid yellow' }}>
      <button onClick={() => setRawArray([...rawArray, { title: '' }])}>Add more</button>

      <SortableTree
        theme={FileExplorerTheme}
        treeData={rawArray}
        onChange={(treeData) => onTreeChange(treeData)}
        generateNodeProps={({ node, path, treeIndex }) => ({
          title: !node.needsTitle ? (
            node.title
          ) : (
              <div>
                <input
                  autoFocus
                  //value={node.title}
                  value={tempTxt}

                  onChange={(e) => onInputChange(node, path, e)}
                />
                <select onChange={(e) => onSelectChange(e)}>
                  <option value='1'>One</option>
                  <option value='2'>Two</option>
                  <option value='3'>Three</option>
                  <option value='4'>Four</option>
                  <option value='5'>Five</option>
                </select>
                <button onClick={(e) => onSubmitForm(node, path, e)} >Save</button>
              </div>
            ),
          buttons: [
            <button
              className="btn"
              id={path.length === 1 ? 'btn_add_parent' : 'btn_add_children'}
              onClick={(e) => onAddNode(node, path, treeIndex, e)}
            >+</button>,
            <button
              className="btn"
              onClick={(e) => removeNode(node, path, e)}
            >-</button>,
          ],
        })}
      />
      <div>Please provide Title</div>
    </div>
  )

}



export default NewSchemaTest2;