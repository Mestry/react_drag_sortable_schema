import React, { useState, useContext, useRef } from 'react';
import SortableTree from 'react-sortable-tree';
import { getNodeAtPath, addNodeUnderParent, removeNodeAtPath, changeNodeAtPath } from "react-sortable-tree";
import { DrageContextProvider, DragContext } from './DragContext';

import NewSchemaTest2 from '../CreateSchema/NewSchemaTest2';

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// const TextField = (props) => {

//     const  { treeData , setTreeData } = useContext(DragContext);

//     const [inputV, setInputV] = useState('');

//     const setTxt = (e) => {

//         console.log('on change id :' + props.id);    

//         //this.setState({ treeData :  [ ...this.state.treeData , this.state.treeData[0].title = 'pgm' ]});

//         let tempTree = [...treeData];

//         console.log('temp tree');
//         console.log(tempTree);

//         if(!isEmpty(tempTree[props.id]))
//         {
//             console.log('one condition');
//             console.log(tempTree[props.id]);
//             tempTree[props.id].title = e.target.value;
//             setInputV(tempTree[props.id].title);

//             setTreeData(tempTree);
//         }
//         else{
//             console.log('two condition');
//             console.log(e.target.value);
//             setInputV(e.target.value);
//         }

//         return;

//     }

//     return (
//         <>
//             <input type="text" value={ treeData[props.id].title } onChange={setTxt} id={`input_${props.id}`} />

//             <select id={`select_${props.id}`}>
//                 <option key='100'>100</option>
//                 <option key='200'>200</option>
//                 <option key='300'>300</option>
//                 <option key='400'>400</option>
//                 <option key='500'>500</option>
//             </select>
//         </>
//     )
// }


// export const SubTree = () => {


//     const  { treeData , setTreeData } = useContext(DragContext);

//     const removeNode = rowInfo => {
//         let { node, treeIndex, path } = rowInfo;

//         let tempAfterRemove = removeNodeAtPath({
//                 treeData: treeData,
//                 path: path,
//                 getNodeKey: ({ node: TreeNode, treeIndex: number }) => {
//                      console.log(number);

//                     return number;
//                 },
//                 ignoreCollapsed: false,
//             })

//         setTreeData(tempAfterRemove);        

//     }

//     const updateTreeData = treeData => {
//         console.log('change tree');
//         console.log(treeData);
//         //setTreeData(treeData);
//     }



//     const addNode = rowInfo => {

//         console.log('add node click : tree index ' + rowInfo.treeIndex);
//         console.log(rowInfo);

//         let NEW_NODE = { title: '' , id: '' };
//         let { node, treeIndex, path } = rowInfo;

//         console.log('path');
//         console.log(path,"  :  ",treeIndex);
//         console.log(node);

//         path.pop();
//         let parentNode = getNodeAtPath({
//             treeData: treeData,
//             path: path,
//             getNodeKey: ({ treeIndex }) => treeIndex,
//             ignoreCollapsed: true
//         });

//         let getNodeKey = ({ node: object, treeIndex: number }) => {
//             return number;
//         };

//         let parentKey = getNodeKey(parentNode);

//         if (parentKey == -1) {
//             parentKey = null;
//         }
//         let newTree = addNodeUnderParent({
//             treeData: treeData,
//             newNode: NEW_NODE,
//             expandParent: true,
//             parentKey: parentKey,
//             getNodeKey: ({ treeIndex }) => treeIndex
//         });
//         setTreeData(newTree.treeData);
//     }


//     return (
//         <div style={{ height: 600, width: 600, border: '1px solid green' }}>

//             <button onClick={() => setTreeData([ ...treeData , { title : 'pgm new' } ] )}>
//                 Add more
//             </button>

//                 <SortableTree
//                     treeData={treeData}
//                     onChange={updateTreeData}

//                     generateNodeProps={(rowInfo) => (
//                         {
//                             buttons: [
//                                 <div style={{ width: '300px' }}>

//                                     <TextField title={rowInfo.node.title} id={rowInfo.treeIndex} info={rowInfo} />

//                                     &nbsp;&nbsp;

//                                 <button onClick={(event) => addNode(rowInfo)}><span>+</span></button>
//                                     &nbsp;&nbsp;
//                                 <button onClick={(event) => removeNode(rowInfo)}><span>-</span></button>

//                                 </div>,
//                             ],
//                             style: {
//                                 height: '50px',
//                             }
//                         })}
//                 />            

//         </div>






//     )
// }




















const SelectField = ({ node, path, treeIndex }) => {

    const { treeData, setTreeData } = useContext(DragContext);
    const [inputV, setInputV] = useState('');

    const inputT = useRef();

    const setTxt = (e) => {

        console.log('on change id : ' + treeIndex);

        //console.log(node);
        //console.log(path);
        // let tempTree = [...treeData];

        // console.log('temp tree');
        // console.log(tempTree);

        console.log(inputT.current.value);
        setInputV(inputT.current.value);

    }

    return (
        <>
            <input type="text" ref={inputT} value={inputV} onChange={setTxt} />
            <select>
                <option key='100'>100</option><option key='200'>200</option><option key='300'>300</option>
                <option key='400'>400</option><option key='500'>500</option>
            </select>
        </>
    )
}
//




const SecondTree = () => {

    const { treeData, setTreeData } = useContext(DragContext);

    const getNodeKey = ({ treeIndex }) => treeIndex;

    const addNewChild = (node, path) => {

        setTreeData(addNodeUnderParent({
            treeData: treeData,
            parentKey: path[path.length - 1],
            expandParent: true,
            getNodeKey,
            newNode: { title: '', id: '' },
        }).treeData)
    }

    const removeField = (path) => {

        setTreeData(removeNodeAtPath({
            treeData: treeData,
            path,
            getNodeKey,
        })
        )
    }

    const changeNodeTitle = (node, path, e) => {

        console.log('path is : ' + path);
        console.log('getNodeKey is : ' + getNodeKey);
        console.log('input change :' + e.target.value);

        let newTreeData = changeNodeAtPath({
            treeData: treeData,
            path: path,
            newNode: { title: e.target.value, id: e.target.value },
            getNodeKey: ({ treeIndex }) => treeIndex
        });

        setTreeData(newTreeData);
    }

    const changeSelect = (node, path, e) => {
        console.log('select change' + e.target.value);
        //console.log('select change'+e.target);

        // let newTreeData = changeNodeAtPath({
        //     treeData: treeData,
        //     path: path,
        //     newNode: { title: e.target.value  , id : e.target.value },
        //     getNodeKey
        // });

        // setTreeData(newTreeData);
    }


    return (
        <div>
            <button onClick={() => setTreeData([...treeData, { title: '...' }])}>Add more</button>

            <div style={{ height: 800, width: 700, border: '1px solid #fff' }}>

                <SortableTree
                    treeData={treeData}
                    onChange={treeData => {
                        console.log('change');
                        setTreeData(treeData);
                    }
                    }
                    generateNodeProps={({ node, path, treeIndex }) => ({
                        buttons:
                            [
                                // <SelectField  node= {node} path={path} id={ treeIndex }/>,    
                                <input type="text" placeholder='add formula name' onChange={(e) => changeNodeTitle(node, path, e)} />,
                                <select onChange={(e) => changeSelect(node, path, e)}>
                                    <option value='1'>One</option>
                                    <option vale='2'>Two</option>
                                    <option vale='3'>Three</option>
                                </select>,
                                <button onClick={() => addNewChild(node, path)} >Add Child</button>,

                                <button onClick={() => removeField(path)} >Remove</button>,
                            ],

                    })}
                />
            </div>


        </div>
    )


}




















const NewSchemaTest1 = () => {

    return (
        <DrageContextProvider>

            {/* <SubTree /> */}

            {/* <SecondTree /> */}

            <NewSchemaTest2 />


        </DrageContextProvider>
    )
}
export default NewSchemaTest1;



