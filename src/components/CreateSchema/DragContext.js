import React, { createContext , useState, useEffect } from 'react';

export const DragContext = createContext();

export const DrageContextProvider = (props) => {

    const [ treeData , setTreeData ] = useState( [{ title: 'root', id: 10 },{ title: 'child-1', id: 11 },{ title: 'child-2', id: 12 }]);


    const [ rawArray , setRawArray ] = useState( [{ title: 'root', id: 0 }]);

    //const [ inputValue , setInputValue ] = useState([{title}])


    useEffect(() =>{
        console.log('change in tree data');

        console.log(treeData);

    },[treeData]);

    return (
        <DragContext.Provider value={{
                        name: 'prashant',

                        treeData : treeData,
                        setTreeData : setTreeData,

                        rawArray : rawArray,
                        setRawArray : setRawArray
        }}>
            {props.children}
        </DragContext.Provider>
    )
}


