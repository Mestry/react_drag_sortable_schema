

// id: 'id-1',
// title: 'item-1', 
// children: [ ]


export let _data = [
    { 
        frml_id: 'bs_ann_sa_051',
        id: 'bs_ann_sa_051',
        name: 'bs_ann_sa_051',
        frml_str : 'item-51', 
        title: 'item-511',
        //data : ['1092' , '2076' , '3564' , '2341'],
        data : []
    },
    {
        frml_id: 'bs_ann_sa_052',
        id: 'bs_ann_sa_052',
        name: 'bs_ann_sa_052',
        title: 'item-52',
        frml_str: 'item-11',
        //data : [ '1092' , '2076' , '3564' , '2341' ],
        data : []
    },
    {
        frml_id: 'bs_ann_sa_053',
        id: 'bs_ann_sa_053', 
        name: 'bs_ann_sa_053', 
        title : 'item-53', 
        frml_str : 'item-111',
        //data : [ '1092' , '2076' , '3564' , '2341' ] ,
        data : []
    },
    { 
        frml_id: 'bs_ann_sa_054', 
        id: 'bs_ann_sa_054', 
        name: 'bs_ann_sa_054', 
        title: 'item-54' , 
        frml_str: 'item-2' , 
        //data : [ '1092' , '2076' , '3564' , '2341' ]
        data : []
    },
    { 
        frml_id: 'bs_ann_sa_055',
        id: 'bs_ann_sa_055',
        name: 'bs_ann_sa_055', 
        title: 'item-55',  
        frml_str: 'item-3',  
        //data : [ '1092' , '2076' , '3564' , '2341' ], 
        data : []
    }    
  ];



export const _data_value = [
    {
        "frml_id": "bs_ann_sa_051",        
        //"frml_str": "Revaluation reserve",
        "frml_data": [
            {                
                "rprt_date": "2019-03-31 00:00:00",
                "value": "121"
            },
            {                
                "rprt_date": "2018-03-31 00:00:00",
                "value": "122"
            }            
        ]        
    },
    {
        "frml_id": "bs_ann_sa_052",
        //"frml_str": "Revaluation reserve",
        "frml_data": [
            {                
                "rprt_date": "2019-03-31 00:00:00",
                "value": "130"
            },
            {                
                "rprt_date": "2018-03-31 00:00:00",
                "value": "131"
            },
            {                
                "rprt_date": "2017-03-31 00:00:00",
                "value": "132"
            }                        
        ]
    },
    {
        "frml_id": "bs_ann_sa_053",
        //"frml_str": "Revaluation reserve",
        "frml_data": [
            {                
                "rprt_date": "2019-03-31 00:00:00",
                "value": "1320"
            },
            {                
                "rprt_date": "2018-03-31 00:00:00",
                "value": "1313"
            },
            {                
                "rprt_date": "2017-03-31 00:00:00",
                "value": "1342"
            }                        
        ]
    },
    {
        "frml_id": "bs_ann_sa_054",
        //"frml_str": "Revaluation reserve",
        "frml_data": [
            {                
                "rprt_date": "2019-03-31 00:00:00",
                "value": "160"
            },
            {                
                "rprt_date": "2018-03-31 00:00:00",
                "value": "161"
            },
            {                
                "rprt_date": "2017-03-31 00:00:00",
                "value": "162"
            }                        
        ]
    },
    {
        "frml_id": "bs_ann_sa_055",
        //"frml_str": "Revaluation reserve",
        "frml_data": [
            {                
                "rprt_date": "2019-03-31 00:00:00",
                "value": "150"
            },
            {                
                "rprt_date": "2018-03-31 00:00:00",
                "value": "151"
            },
            {                
                "rprt_date": "2017-03-31 00:00:00",
                "value": "152"
            }                        
        ]
    }              
]
















// export const userSchema = 
// [
//     { frml_id : 'bs_ann_sa_011', frml_str : 'Revaluation 11' , 
//         children : [            
//             { frml_id : 'bs_ann_sa_0111', frml_str : 'Revaluation 111' },
//             { frml_id : 'bs_ann_sa_0112', frml_str : 'Revaluation 112' }            
//         ] 
//     },
//     { frml_id : 'bs_ann_sa_012', frml_str : 'Revaluation 12' },  
//     { frml_id : 'bs_ann_sa_013', frml_str : 'Revaluation 13' },
//     { frml_id : 'bs_ann_sa_014', frml_str : 'Revaluation 14' },  

// ];

// export const rootArray = [
//     { frml_id : 'bs_ann_sa_011', frml_str : 'Revaluation 11' },
//     { frml_id : 'bs_ann_sa_012', frml_str : 'Revaluation 12' },
//     { frml_id : 'bs_ann_sa_013', frml_str : 'Revaluation 13' },
//     { frml_id : 'bs_ann_sa_014', frml_str : 'Revaluation 14' },
//     { frml_id : 'bs_ann_sa_015', frml_str : 'Revaluation 15' },
//     { frml_id : 'bs_ann_sa_016', frml_str : 'Revaluation 16' },
//     { frml_id : 'bs_ann_sa_017', frml_str : 'Revaluation 17' }
// ];
