export const LD = [
    {
        id : 1,
        name : 'LEFTPARENTHESIS',
        value : /^\(/
    },
    {
        id : 2,
        name : 'RIGHTPARENTESIS',
        value : /^\)/
    },
    {
        id : 3,
        name : 'ARITHMETICOPERATOR',
        value : /^[\+\-*\/]/
    }
]