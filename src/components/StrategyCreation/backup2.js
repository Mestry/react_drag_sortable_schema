import React, {useState, useContext, useEffect, useRef} from 'react'
import styled from 'styled-components'
import ContentEditable from 'react-contenteditable'
import {MyThemeContext} from '../../contexts/MyThemeContext'
import { getCaretTopPoint } from './caretPos'
import DD from './DD'

import { DatePicker } from 'antd';

const {RangePicker} = DatePicker;

const MainDDList = ['apple', 'asterisk', 'barcelona', 'brew', 'beetles', 'cranberries', 'cyan', 'darkside', 'deeppurple']


const Strategy = () => {
    const {theme} = useContext(MyThemeContext)
    const [html, setHtml] = useState('')
    const [showDD, setShowDD] = useState(false)
    const [ddPositionLeft, setDdPositionLeft] = useState(0)
    const [ddPositionTop, setDdPositionTop] = useState(0)
    const [searchString, setSearchString] = useState('')

    const [expecting, setExpecting] = useState('dropdown')

    const contentEditable = useRef(null)

    const handleCeChange = (e) => {
        setHtml(e.target.value)
        console.log(e)
        setShowDD(true)
        console.log("searchString", searchString)
        if(e.nativeEvent.data && e.nativeEvent.data.match(/[a-z]/g)){
            setSearchString(searchString + e.nativeEvent.data)
            setShowDD(true)
        } else if(e.nativeEvent.inputType === 'deleteContentBackward') {
            setSearchString(searchString.substring(0, searchString.length - 1))
            //setShowDD(false)
        } else if(e.nativeEvent.data && e.nativeEvent.data === " "){
            setSearchString('')
        }
    }

    const handleDDClick = (inp) => {
        let el = contentEditable.current
        contentEditable.current.focus()

        if (typeof window.getSelection != "undefined"
                && typeof document.createRange != "undefined") {
            var range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if (typeof document.body.createTextRange != "undefined") {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }

        //alert('selected')
        for(let i=0; i < searchString.length; i++){
            document.execCommand("delete", false, null);
        }
        document.execCommand("bold", false, null)
        document.execCommand("foreColor", false, "turquoise")
        //document.execCommand("insertText", false , inp)
        document.execCommand("bold", false, null)
        document.execCommand("foreColor", false, "grey")
        document.execCommand("insertText", false , " ")
        document.execCommand("insertHTML", false, `<input type="text" disabled style="text-align:center;border:1px solid #dfdfdf;background-color:#fafafa;width:${(inp.length + 2)*8}px" value=${inp} />`)
        //document.execCommand("insertHTML", false, `<div contenteditable="false">${inp}</div>`)
        setShowDD(false)
    }


    useEffect(() => {
        console.log(document.getSelection())
        contentEditable.current.focus()
        setDdPositionTop(getCaretTopPoint().top)
        setDdPositionLeft(getCaretTopPoint().left)
        console.log(ddPositionTop)
        let found = false
        MainDDList.map((mdi) => {
            if(mdi.includes(searchString)){
                found = true
            }
        })
        if(!found){
            document.execCommand("delete", false, null)
        }
    }, [html])


    return(
        <StyledStrategyCon>
            <StyledContentEditable
                innerRef={contentEditable}
                theme={theme}
                html={html}
                onChange={handleCeChange}
                //onBlur={() => setShowDD(false)}
                />
            <StyledDropDownDiv theme={theme} showDD={showDD} top={ddPositionTop} left={ddPositionLeft}>
                <DD handleDDClick={handleDDClick} searchString={searchString} expecting={expecting}/>
                {/* <RangePicker style={{position : "absolute", width : "300px" ,display : `${showDD ? "block" : "none"}`}}/> */}
            </StyledDropDownDiv>

            <button style={{color : theme.bgDark  ,width : "100px", marginTop : "1em", borderRadius : "4px"}} onClick={() => {
                contentEditable.current.focus()
                document.execCommand("selectAll", false, null)
                document.execCommand("delete", false, null)
            }}>Clear</button>

        </StyledStrategyCon>
    )
}



const StyledStrategyCon = styled.div`
    display : flex;
    flex-direction : column;
    padding : 1em;
`


const StyledContentEditable = styled(ContentEditable)`
    background-color : #fafafa;
    height : 200px;
    width : 500px;
    font-size : 1.2em;
    border-radius : 4px;
    padding : 1em;
    border : 1px solid #d9d9d9;
    color : grey

    &:focus {
        outline : none
    }


    // &:focus {
    //     padding : 0em 0 0 1em;
    // }
`


const StyledDropDownDiv = styled.div`
    display : ${props => props.showDD ? "flex" : "none"};
    flex-direction : column-reverse;
    position : absolute;
    top : ${ props => props.top}px;
    left : ${ props => props.left}px;
   

`


export default Strategy