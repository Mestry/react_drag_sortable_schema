import React, {useState, useEffect, useRef} from 'react'
import styled from 'styled-components'
import {AutoComplete} from 'antd'


const MainDDList = ['apple', 'asterisk', 'barcelona', 'brew', 'beetles', 'cranberries', 'cyan', 'darkside', 'deeppurple']

const OperatorList = ['>' , '<' , '=']

const DropDown = (props) => {

    const [displayDD, setDisplayDD] = useState([])
    const acRef = useRef(null)

    
    useEffect(() => {
        console.log(props.searchString, props.expecting)
        if(props.expecting === 'dropdown'){
            setDisplayDD(MainDDList.filter((mdi) => { return mdi.includes(props.searchString)}))
        } else if(props.expecting === 'operator'){
            setDisplayDD(OperatorList.filter((mdi) => { return mdi.includes(props.searchString)}))
        }
        if(acRef){
           acRef.current.focus() 
        }
    }, [props.expecting, props.searchString])

    return (
        <StyledDDCon ss={props.searchString}>
            <AutoComplete
                ref={acRef}
                //value={value}
                dataSource={MainDDList.filter((mdi) => { return mdi.includes(props.searchString)})}
                style={{ width: 300, opacity : "0", marginTop : "-15px" }}
                onSelect={(val) => props.handleDDClick(val)}
                //onSearch={this.onSearch}
                //onChange={this.onChange}
                open={props.searchString !== '' ? true : false}
                placeholder="control mode"
            />
        </StyledDDCon>
    )

}

const StyledDDCon = styled.div`
    //border : 1px solid #dfdfdf;
    margin-top : 0px;
    opacity : ${props => props.ss === '' ? "0" : "1"}
    color : black
`

export default DropDown