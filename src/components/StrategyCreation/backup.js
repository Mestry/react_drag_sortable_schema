import React, {useRef, useState, useEffect, useCallback} from 'react'
import {AutoComplete} from 'antd'
import styled from 'styled-components'
import ContentEditable from 'react-contenteditable'
import {LD} from './lexicalDict'

const dataSourceMain = ['apple', 'asterisk', 'barcelona', 'brew', 'beetles', 'cranberries', 'cyan', 'darkside', 'deeppurple']

const Strategy = () => {

    const contentEditable = useRef(null);
    const ddRef = useRef(null)

    const [html, setHtml] = useState('<div>hi</div>')
    const [dataSource, setDataSource] = useState(dataSourceMain)
    const [openDropDown, setOpenDropDown] = useState(false)
    const [positionTop, setPositionTop] = useState(0)
    const [positionLeft, setPositionLeft] = useState(0)

    const handleChange = (e) => {
        console.log(e)
        setHtml(e.target.value)
    }

    useEffect(() => {
        contentEditable.current.focus()
    })


    const getCaretTopPoint = () => {
        const sel = document.getSelection()
        const r = sel.getRangeAt(0)
        let rect
        let r2
        // supposed to be textNode in most cases
        // but div[contenteditable] when empty
        const node = r.startContainer
        const offset = r.startOffset
        if (offset > 0) {
          // new range, don't influence DOM state
          r2 = document.createRange()
          r2.setStart(node, (offset - 1))
          r2.setEnd(node, offset)
          // https://developer.mozilla.org/en-US/docs/Web/API/range.getBoundingClientRect
          // IE9, Safari?(but look good in Safari 8)
          rect = r2.getBoundingClientRect()
          return { left: rect.right, top: rect.top }
        } else if (offset < node.length) {
          r2 = document.createRange()
          // similar but select next on letter
          r2.setStart(node, offset)
          r2.setEnd(node, (offset + 1))
          rect = r2.getBoundingClientRect()
          return { left: rect.left, top: rect.top }
        } else { // textNode has length
          // https://developer.mozilla.org/en-US/docs/Web/API/Element.getBoundingClientRect
          rect = node.getBoundingClientRect()
          const styles = getComputedStyle(node)
          const lineHeight = parseInt(styles.lineHeight)
          const fontSize = parseInt(styles.fontSize)
          // roughly half the whitespace... but not exactly
          const delta = (lineHeight - fontSize) / 2
          return { left: rect.left, top: (rect.top + delta) }
        }
      }



    const onAcChange = (val) => {
        console.log(val)
       
        setDataSource(dataSourceMain.filter((dsm) => {
             return dsm.includes(val)
        }))
    }


    const onSelect = (val) => {
        console.log('heyy', val)
        contentEditable.current.focus()
        document.execCommand('delete', false)
        document.execCommand('insertHTML', false, `&nbsp;`);
        document.execCommand('insertHTML', false, `<div style="color : blue">${val}</div>`);
        setOpenDropDown(false)
    }


    const delElem = () => {
        document.execCommand('delete', false)
    }

    useEffect(() => {
        console.log(html, html.search('@'))
        if(html.search('@') >= 0){
            setOpenDropDown(true)
            console.log(getCaretTopPoint())
            setPositionLeft(getCaretTopPoint().left)
            setPositionTop(getCaretTopPoint().top)
        }

        // if(html.search('a') >= 0){
        //     document.execCommand('delete', false)
        // }

        // let a = contentEditable.current;
        // let d = ddRef.current;
        // let b=  document.body

        // let sel = document.getSelection();
        // let targ = sel.getRange();
        // setPositionTop(a.offsetTop+a.clientTop-d.clientHeight-6);
        // setPositionLeft(d.style.left=targ.offsetLeft+b.scrollLeft-6);

        //console.log(getCaretTopPoint())


    }, [html])


    return(
        <StyledStrategyCon>
            <StyledContentEditable
              innerRef={contentEditable}
              html={html} // innerHTML of the editable div
              disabled={false}       // use true to disable editing
              onChange={handleChange} // handle innerHTML change
              tagName='article' // Use a custom HTML tag (uses a div by default)
            />
            {/* <AutoComplete
                //value={value}
                dataSource={dataSource}
                style={{ width: 200 }}
                onSelect={onSelect}
                //onSearch={onSearch}
                onChange={onAcChange}
                open={openDropDown}
                placeholder="control mode"
            /> */}

       
                <div ref={ddRef} style={{position : "absolute", display : "flex", flexDirection : "column",
                    left :  `${positionLeft}px`, top : `${positionTop + 20}px`
                }}>
                    {dataSource.map((dsi, i) => <button  onClick={() => onSelect(dsi)} key={i}>{dsi}</button>)}
                </div>
             
            {/* <button onClick={delElem}>Del</button> */}
        </StyledStrategyCon>
    )
}



const StyledStrategyCon = styled.div`
    width : 100%;
    height : 10vh;
    line-height : 1.5;
    color : grey;
    padding : 1em;
    margin-top : 5em
`

const StyledContentEditable = styled(ContentEditable)`
    & div {
        background-color : cyan;
        width : 100%;
    }
`

export default Strategy