import React, {useRef, useEffect} from 'react'
import {AutoComplete} from 'antd'
import { DatePicker } from 'antd';

const {RangePicker} = DatePicker;

const MainDDList = ['apple', 'asterisk', 'barcelona', 'brew', 'beetles', 'cranberries', 'cyan', 'darkside', 'deeppurple']
const OperatorsList = ['>' , '<', '=']

const DD = (props) => {

    let acRef = useRef(null)

    useEffect(() => {
        console.log(props.searchString, "kkkkkk")
    })

    return(
        <div style={{display : "flex", flexDirection : "column"}}>
            {/* {MainDDList.filter((mdi) => { return mdi.includes(props.searchString)}).map((ddi) => {
                return (
                    <div onClick={() => props.handleDDClick(ddi)}>{ddi}</div>
                )
            })} */}
            <AutoComplete
                //value={value}
                ref={node => acRef = node}
                dataSource={
                    props.expecting === 'dropdown' ? 
                    MainDDList.filter((mdi) => { return mdi.includes(props.searchString)})
                    :
                    OperatorsList.filter((mdi) => { return mdi.includes(props.searchString)})
                }
                style={{ width: 300, opacity : "0", marginTop : "-15px" }}
                onSelect={(val) => props.handleDDClick(val)}
                //onSearch={this.onSearch}
                //onChange={this.onChange}
                open={props.searchString !== '' ? true : false}
                placeholder="control mode"
            />
            
        </div>
    )
}

export default DD