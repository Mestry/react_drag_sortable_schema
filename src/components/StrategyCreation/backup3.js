import React, {useState, useRef, useEffect} from 'react'
import ContentEditable from 'react-contenteditable'
import DropDown from './DropDown'
import styled from 'styled-components'
import { getCaretTopPoint, getCaretPosition } from './caretPos'



const Strategy = () => {

    const ceRef = useRef(null)

    const [html, setHtml] = useState('')
    const [searchString, setSearchString] = useState('')
    const [expecting, setExpecting] = useState('dropdown')
    const [ddPositionLeft, setDdPositionLeft] = useState(0)
    const [ddPositionTop, setDdPositionTop] = useState(0)
    const [caretPos, setCaretPos] = useState(0)

    

    const handleCeChange = (e) => {
        
        setHtml(e.target.value)

        let sel = document.getSelection()
        //sel.removeAllRanges();
        let range = sel.getRangeAt(0)

        setCaretPos(range.startOffset)

        console.log(caretPos.offset)
        if(e.nativeEvent.data && e.nativeEvent.data.match(/[a-z]/g)){
            setSearchString(searchString + e.nativeEvent.data)
        } else if(e.nativeEvent.data && e.nativeEvent.data.match(/</g)){
            setSearchString(searchString + e.nativeEvent.data)
        } else if(e.nativeEvent.inputType === 'deleteContentBackward') {
            setSearchString(searchString.substring(0, searchString.length - 1))
        } else if(e.nativeEvent.data && e.nativeEvent.data === " "){
            setSearchString('')
        }

    }

    useEffect(() => {
        ceRef.current.focus()
        setDdPositionTop(getCaretTopPoint().top)
        setDdPositionLeft(getCaretTopPoint().left)
    })


    useEffect(() => {
        
    }, [html])


    // function insertTextAtCursor(text) {
    //     var sel, range;
    //     if (window.getSelection) {
    //         sel = window.getSelection();
    //         if (sel.getRangeAt && sel.rangeCount) {
    //             range = sel.getRangeAt(0);
    //             range.deleteContents();
    //             range.insertNode( document.createTextNode(text) );
    //         }
    //     } else if (document.selection && document.selection.createRange) {
    //         document.selection.createRange().text = text;
    //     }
    // }
    




    const handleDDClick = (inp) => {
        
        let el = ceRef.current
        ceRef.current.focus()

        //let sel = document.getSelection()
        //sel.removeAllRanges();
       // let range = sel.getRangeAt(0)

        // let newRange = document.createRange();

        // newRange.setStart(el, caretPos)
        // newRange.setEnd(el, caretPos)

        if (typeof window.getSelection != "undefined"
                && typeof document.createRange != "undefined") {
                    console.log("yeahhhh")
            var range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
            
        } else if (typeof document.body.createTextRange != "undefined") {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }

        //alert('selected')

        for(let i=0; i < searchString.length; i++){
            document.execCommand("delete", false, null);
        }


        document.execCommand("bold", false, null)
        document.execCommand("foreColor", false, "turquoise")
        //document.execCommand("insertText", false , inp)
        document.execCommand("bold", false, null)
        document.execCommand("foreColor", false, "grey")
        //document.execCommand("insertText", false , " ")
        document.execCommand("insertHTML", false, `<input type="text" disabled style="text-align:center;border:1px solid #dfdfdf;background-color:#fafafa;width:${(inp.length + 2)*8}px" value=${inp} />`)
        setSearchString('')
        if(expecting === 'dropdown'){
            setExpecting('operator')
        }



    }



    return(
        <StyledStrategyCon>
            <StyledContentEditable
                innerRef={ceRef}
                html={html} // innerHTML of the editable div
                disabled={false}       // use true to disable editing
                onChange={handleCeChange} // handle innerHTML change
                tagName='article' // Use a custom HTML tag (uses a div by default)
            />
            <StyledDropDownDiv  top={ddPositionTop} left={ddPositionLeft}>
                <DropDown handleDDClick={handleDDClick} searchString={searchString} expecting={expecting}/>
            </StyledDropDownDiv>
        </StyledStrategyCon>
        
    )


}

const StyledStrategyCon = styled.div`
    display : flex;
    flex-direction : column;
    padding : 1em;
`


const StyledContentEditable = styled(ContentEditable)`
    background-color : #fafafa;
    height : 200px;
    width : 500px;
    font-size : 1.2em;
    border-radius : 4px;
    padding : 1em;
    border : 1px solid #d9d9d9;
    color : grey

    &:focus {
        outline : none
    }


    // &:focus {
    //     padding : 0em 0 0 1em;
    // }
`


const StyledDropDownDiv = styled.div`
    flex-direction : column-reverse;
    position : absolute;
    top : ${ props => props.top}px;
    left : ${ props => props.left}px;
   

`



export default Strategy