import React, { useState } from 'react'
import styled from 'styled-components'
import {Editor, EditorState, RichUtils} from 'draft-js';


const Strategy = () => {


    const [editorState, setEditorState] = useState(EditorState.createEmpty())

    const onChange = (editorState) => {

        console.log(editorState)


        setEditorState(editorState)
    }

    const _onBoldClick = () => {
        onChange(RichUtils.toggleInlineStyle(editorState, 'BOLD'));
    }

    

    return(
        <StyledStrategyBuilderCon>
            <h1>Strategy Builder</h1>
            <StyledEditorCon>
                <Editor 
                    editorState={editorState} 
                    onChange={onChange}
                    placeholder="please enter the formula below"
                    textAlignment="center"
                />
            </StyledEditorCon>
            <button style={{color : "black", marginTop : "1em"}} onClick={_onBoldClick}>Bold</button>

        </StyledStrategyBuilderCon>
        
    )

}

const StyledStrategyBuilderCon = styled.div`
    color : lightgrey
    padding : 1em;
    & h1 {
        color : lightgrey
    }
`

const StyledEditorCon = styled.div`
    background-color : #dfdfdf
    color : black;
    padding : 1em;
`


export default Strategy