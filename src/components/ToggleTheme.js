import React, {useContext, useState} from 'react'
import styled from 'styled-components'
import { MyThemeContext } from '../contexts/MyThemeContext'
import { BackTestContext } from '../contexts/BackTestContext'
import Switch from "react-switch";


const ToggleTheme = () => {
    const {theme, currentTheme, toggleTheme} = useContext(MyThemeContext)
    const [checked, setChecked] = useState(false)

    console.log(theme.bg)

    const handleChange = (checked) => {
        console.log("checked")
        setChecked(checked);
        if(currentTheme.currentTheme === "light"){
            toggleTheme("dark")
        } else if(currentTheme.currentTheme === "dark") {
            toggleTheme("light")
        }
    }

    return(
        <div style={{display : "flex", height : "100%", alignItems : "center", marginRight : "0.8em"}}>
            <Switch onChange={handleChange} height={23} width={40} offColor="#888" onColor={theme.text} checkedIcon={false} uncheckedIcon={false} checked={checked} />
        </div>
    )
}

export default ToggleTheme