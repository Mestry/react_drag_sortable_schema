import React, {useContext} from 'react'
import {Select} from 'antd'
import styled from 'styled-components'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'
const {Option} = Select

const SelectStrategy = () => {
    const {theme} = useContext(MyThemeContext)
    const {strategy, handleStrategy} = useContext(BackTestContext)

    return(
        <React.Fragment>
            <StyledSelect theme={theme} defaultValue="Manual" value={strategy} onChange={(val) => {handleStrategy(val)}}>
                <Option value="Manual">Manual</Option>
                <Option value="High Value">High Value</Option>
                <Option value="Momentum Driven">Momentum Driven</Option>
            </StyledSelect>
            <CreateStrategyButton theme={theme}> + Create new strategy </CreateStrategyButton>
        </React.Fragment>
     )
}


const StyledSelect = styled(Select)`
    width : 100%;
    & .ant-select-selection{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text};
        border-color : ${props => props.theme.text};
    };
    & .anticon{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text}
    };
`


const CreateStrategyButton = styled.div`
    border : 1px solid ${props => props.theme.text};
    background-color : ${props => props.theme.bgDark};
    border-radius : 4px;
    margin-top : 0.5em;
    font-size : 0.8em;
    padding : 0.2em 0.8em ;
    display : inline-block;
    cursor : pointer;

    &:hover {
        background-color : ${props => props.theme.bg};  
    }
`

export default SelectStrategy