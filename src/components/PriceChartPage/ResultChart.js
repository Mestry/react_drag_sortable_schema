import React, {useState, useContext, useEffect, useRef} from 'react'
import {Line} from 'react-chartjs-2'
import {Slider} from 'antd'
import styled from 'styled-components'
import { BackTestContext } from '../../contexts/BackTestContext'
import * as moment from 'moment'
import 'chartjs-plugin-datalabels'


const ResultChart = () => {
    const {backTestResults , dateRange ,handleSelectedDataPoint, startingAmount ,transactions} = useContext(BackTestContext)
    const [allDataPoints, setAllDataPoints] = useState([])
    const [allDataPoints2, setAllDataPoints2] = useState([])
    const [allLabels, setAllLabels] = useState([])
    const [dataStart, setDataStart] = useState(0)
    const [dataEnd, setDataEnd] = useState(0)
    const [labelStart, setLabelStart] = useState(0)
    const [labelEnd, setLabelEnd] = useState(0)
    const [ted, setTed] = useState(0)
    const [highlightIndex, setHighlightIndex] = useState(0)
    const [actualHighlightIndex, setActualHighlightIndex] = useState(0)
    const [isRunning, setIsRunning] = useState(false);
    const [incrementBy, setIncrementBy] = useState(1)


    const ccRef2 = useRef(null);
    let chartReference2 = useRef(null);

    useInterval(() => {
        if(dataEnd >= ted){
            setIsRunning(false)
        }

        setDataEnd(dataEnd + incrementBy)
    }, isRunning ? 15 : null);

    useEffect(() => {
        console.log(actualHighlightIndex)
        handleSelectedDataPoint(actualHighlightIndex)
    }, [actualHighlightIndex, handleSelectedDataPoint])

    useEffect(() => {
        //clearInterval(intervalId);
        let labels = [];
        let dataPoints = [];
        let dataPoints2 = [];
        let ted = 0;
        (backTestResults.daily_market_value || []).map((ad) => {
          labels.push(ad.date)
          dataPoints.push(ad.price)
          dataPoints2.push((parseFloat(ad.market_value) + parseFloat(startingAmount) + parseFloat(ad.net_cashflow) ))
          let m_ad_date = moment(ad.date, "YYYY-MM-DD")
          let m_single_date = moment(dateRange.endingDate, "YYYY-MM-DD")
          let diff = m_ad_date - m_single_date
          if(diff < 0){
            ted = ted + 1;
          }
        })
        setAllDataPoints(dataPoints)
        setAllDataPoints2(dataPoints2)
        setAllLabels(labels)
        setLabelEnd(ted)
        setTed(ted)
        setIsRunning(true)
        let i;
          if(backTestResults.daily_market_value.length >= 100){
            setIncrementBy(Math.floor(backTestResults.daily_market_value.length/100))
          } else {
            setIncrementBy(1)
        }
        console.log("lllllpppppp")
    }, [backTestResults, dateRange, startingAmount])



    const data = {
        labels : allLabels.slice(labelStart, labelEnd),
        datasets : [{
          label  : 'price',
          data : allDataPoints.map((dt) => (dt/allDataPoints[dataStart] - 1)).slice(dataStart, dataEnd),
          fill : false,
          borderColor  : "#1890ff"
        },
        {
          label  : 'Market Value',
          data : allDataPoints2.map((dt) => (dt/allDataPoints2[dataStart] -1 )).slice(dataStart, dataEnd),
          fill : false,
          borderColor  : "orange"
        }]
    }

    const options={
        animation : {
          duration : 0
        },
         hover: {
            mode: "nearest",
            intersect: false,
            axis : 'x'
          },
        elements: {
          line : {
            backgroundColor : "#1890ff",
            borderColor  : "#1890ff"
          },
          point: {
            hoverRadius : (context) => {
                let hrad = 4;
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate && context.datasetIndex === 0){
                        hrad = 7
                    }
                })
                if(context.dataIndex === highlightIndex){
                    hrad = 7
                }
                return hrad
            },
            radius : (context) => {
                let rad = 0;
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate && context.datasetIndex === 0){
                        rad = 7
                    }
                })
                if(context.dataIndex === highlightIndex){
                    rad = 7
                }
                return rad
            },
            borderWidth : 0,
            display: true,
            pointStyle : (context) => {
            let ps = 'circle';
            let contextCompany = context.dataset.label
            let contextDataIndex = context.dataIndex
            let contextDate = context.chart.config.data.labels[contextDataIndex]
            transactions.map((trxn) => {
                if(trxn.date === contextDate && context.datasetIndex === 0){
                    ps = 'triangle'
                }
            })
            return ps
            },
            backgroundColor : (context) => {
            let bc = '#1890ff';
            let contextCompany = context.dataset.label
            let contextDataIndex = context.dataIndex
            let contextDate = context.chart.config.data.labels[contextDataIndex]
            transactions.map((trxn) => {
                if(trxn.date === contextDate && trxn.buyOrSell === 'buy'){
                    bc = '#148f42'
                } else if(trxn.date === contextDate && trxn.buyOrSell === 'sell') {
                    bc = '#ff003c'
                }
            })
            return bc
            }
        }
        },
        plugins : {
            datalabels: {
                clamp : true,
                anchor : 'center',
                align : 'end',
                offset : 20,
                color: '#eee',
                borderRadius : 20,
                font : {
                weight : 'bold',
                },
                borderWidth : 2,
                backgroundColor : (context) => {
                let color = '#ff003c';
                let contextCompany = context.dataset.label
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate  && trxn.buyOrSell === 'buy'){
                    color = '#148f42'
                    } else if(trxn.date === contextDate && trxn.buyOrSell === 'sell'){
                    color = '#ff003c'
                    }
                })
                return color
                },
                rotation : 0,
                display : (context) => {
                let found = false;
                let contextCompany = context.dataset.label
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate && context.datasetIndex === 0){
                    found = true
                    }
                })
                return found
                },
                formatter: (value, context) => {
                let ren = 'B';
                let contextCompany = context.dataset.label
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate && trxn.buyOrSell === 'buy'){
                    
                    ren = 'B'
                    } else if(trxn.date === contextDate && trxn.buyOrSell === 'sell'){
                    ren = 'S'
                    }
                })
                return ren
                },
                listeners : {
                click : function(context){
                    alert('clicked')
                }
                }
            }
        },
        tooltips : {
            mode : 'index',
            intersect : false,
            callbacks : {
              label : (ti, d) => {
                console.log(ti,d)
                if(ti.datasetIndex === 0){
                  return ((parseFloat(ti.datasetIndex) + 1) * allDataPoints[ti.index]).toFixed(2)
                }
                if(ti.datasetIndex === 1){
                  return ((parseFloat(ti.datasetIndex) + 1) * allDataPoints2[ti.index]).toFixed(2)
                }
              }
            }
        },
        onHover : (a,b,c) => {
          const god = ccRef2.current.getBoundingClientRect()
          const godX = god.left
          const meX = a.clientX-godX
          const ctx = ccRef2.current.getContext("2d");
          ctx.clearRect(0, 0, god.width, god.height);
          ctx.beginPath();
          ctx.moveTo(meX, 0);
          ctx.lineTo(meX, god.height);
          ctx.strokeStyle = "#333";
          ctx.stroke();
        },
        onClick : (event, elems) => {
            // console.log(event,elems)
            // let chartElem = elems
            // console.log(chartElem)
            // if(chartElem !== undefined && chartElem.length !== 0){
            // setActualHighlightIndex(chartElem[0]._index + dataStart)
            // setHighlightIndex(chartElem[0]._index)
            // }
        },
        scales: {
            xAxes: [
                {
                position: 'bottom',
                id: 'bottom-x-axis',
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: "Date"
                },
                ticks : {
                    callback : function(value, index, values){
                    return moment(value, "YYYY-MM-DD").format("MMM-YY");
                    }
                }
                }
            ],
            yAxes: [
                {
                position: 'left',
                id: 'left-y-axis',
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: "Closing Price"
                }
                }
            ]
        },
    }

    return(
        <StyledPriceChartCon tabIndex="0" 
        >
            <table style={{width : "100%", borderBottom : "1px solid #d9dad9" ,marginBottom : "1em"}}>
              <tbody>
                  <tr style={{padding : "0.5em"}}>
                    <td style={{padding : "0.5em", textAlign : "center"}} span={12}>Closing Cash Availible</td>
                    <td style={{padding : "0.5em", textAlign : "center"}} span={12}>{backTestResults.current_cash_value.toFixed(2)}</td>
                    <td style={{padding : "0.5em", textAlign : "center"}} span={12}>Value of Stock Held</td>
                    <td style={{padding : "0.5em", textAlign : "center"}} span={12}>{backTestResults.current_stock_value.toFixed(2)}</td>
                  </tr>
                  {/* <tr><td span={12}>Value of Stock Held</td><td span={12}>{this.props.btApiData.current_stock_value}</td></tr> */}
                  <tr className="modal-table-tr">
                    <td style={{padding : "0.5em", textAlign : "center"}} span={12}>Total Invested Amount</td>
                    <td style={{padding : "0.5em", textAlign : "center"}}span={12}>{backTestResults.total_invested_amount.toFixed(2)}</td>
                    <td style={{ fontWeight : "bold", padding : "0.5em", textAlign : "center"}} span={12}>Annualized Return (XIRR)</td>
                    <td style={{fontSize : "1.2em", fontWeight : "bold", padding : "0.5em", textAlign : "center"}}  span={12}>{(backTestResults.xirr*100).toFixed(2)} %</td>
                  </tr>
                  {/* <tr><td span={12}>Annualized Return (XIRR)</td><td span={12}>{this.props.btApiData.xirr} %</td></tr> */}
              </tbody>
          </table>
            <div>
                <div style={{position : "relative"}}>
                <canvas ref={ccRef2}  style={{position : "absolute"}} width={800} height={400} />
                <div style={{width : "800px", position : "relative"}}>
                    <Line ref={(reference) => {chartReference2 = reference} }
                        width={800}
                        height={400}
                        data={data}
                        options={options}
                    />
                </div>
                </div>
                <Slider
                    style={{width : "800px"}}
                    range onChange={(val) => {
                        setLabelStart(val[0]);
                        setLabelEnd(val[1]);
                        setDataStart(val[0]);
                        setDataEnd(val[1]);
                    }} 
                    value={[dataStart, dataEnd]}
                    defaultValue={[0, 4000]}
                    min={0}
                    max={ted} 
                    tipFormatter={(value) => {
                        return (
                            allLabels[value]
                        )
                    }}
                    />
            </div>
            {/* <div style={{display : "flex", flexDirection : "column"}}>
                <Button onClick={() => {
                    setHighlightIndex(highlightIndex - 1)
                    setActualHighlightIndex(actualHighlightIndex - 1)
                }}>
                <Icon className="btn-icon-lr"  type="left-circle" />
                </Button>
                <Button style={{marginTop : "0.5em"}} onClick={() => {
                    setHighlightIndex(highlightIndex + 1)
                    setActualHighlightIndex(actualHighlightIndex + 1)
                }}>
                <Icon className="btn-icon-lr"  type="right-circle" />
                </Button>
            </div> */}
        </StyledPriceChartCon>
    )
}


const StyledPriceChartCon = styled.div`
    margin-top : 1em;
    width : 100%;
    padding-top : 0.5em;
    background-color: #fafafa;
    display : flex;
    flex-direction : column;
    align-items: center;
    justify-content: center;
    border-radius: 4px;
    border : 1px solid #d9d9d9;

`

function useInterval(callback, delay) {
    const savedCallback = useRef();
  
    // Remember the latest callback.
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);
  
    // Set up the interval.
    useEffect(() => {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }, [delay]);
  }

export default ResultChart