import React, {useContext, useState, useEffect} from 'react'
import {message, Typography, Table, Row, Radio, Col, InputNumber, Button} from 'antd'
import styled from 'styled-components'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'
import * as moment from 'moment'
const { Title } = Typography;

const BuySellForm = () => {

    const {priceData, selectedDataPoint, selectedCompany, startingAmount ,currentCash, currentQuantity, transactions ,handleTransactions} = useContext(BackTestContext)

    const [buyOrSell , setBuyOrSell] = useState("buy")
    const [quantityOrAmount, setQuantityOrAmount] = useState("quantity")
    const [stocksWeight, setStocksWeight] = useState(1)
    const [transactionInProgress, setTransactionInProgress] = useState(true)
    const [currentTransaction, setCurrentTransaction] = useState(null)

    const error = (errormsg) => {
        message.config({
            top : '90%'
        })
        message.error(errormsg);
    };

    const info = (info_msg) => {
        message.config({
            top : '90%'
        })
        message.info(info_msg);
    }

    // useEffect(() => {

    // }, [startingAmount])

    useEffect(() => {
       
        //console.log(this.props.selectedDataElement)
        //console.log('notttt', prevProps.selectedDataElement, this.props.selectedDataElement)
        let selectedDataElementExistsInTransactions = false
        let l_current_transaction = null;
    
        transactions.map((trxn) => {
            if(trxn.date === getData("date")){
                selectedDataElementExistsInTransactions = true;
                l_current_transaction = trxn;
            }
        })
        if(!selectedDataElementExistsInTransactions){
            setTransactionInProgress(true)
        } else {
            setTransactionInProgress(false)
            setCurrentTransaction(l_current_transaction)
            setBuyOrSell(l_current_transaction.buyOrSell)
            setQuantityOrAmount('quantity')
            setStocksWeight(l_current_transaction.quantity)
            // this.setState({
            //     transactionInProgress : false,    
            //     currentTransaction : l_current_transaction,
            //     buyOrSell : l_current_transaction.buyOrSell,
            //     quantityOrAmount : 'quantity',
            //     stocksWeight : l_current_transaction.quantity
            // })
        }
     
        // if( prevProps.startingAmount !== this.props.startingAmount){
        //     this.setState({
        //         startingAmount : this.props.startingAmount,
        //         currentCash : this.props.startingAmount
        //     })
        // }
    }, [selectedDataPoint, transactions])

    const getAmount = () => {
        let closingPrice = getData("cp");
        if(quantityOrAmount === 'quantity'){
            return stocksWeight * (closingPrice)
        } else {
            return Math.floor(stocksWeight/closingPrice)*closingPrice
        }
    }


    const getQuantity = () => {
        let closingPrice = getData("cp");
        if(quantityOrAmount === 'quantity'){
            return stocksWeight
        } else {
            return Math.floor(stocksWeight/closingPrice)
        }
    }


    const resolveCumulative =  (itrxn) => {
        let cumulative = 0;
        let numOfDatesLess = 0;
        transactions.map((trxn, i) => {
            let a = moment(trxn.date, "YYYY-MM-DD");
            let b = moment(getData("date"), "YYYY-MM-DD");

            if(a.diff(b) < 0){
                numOfDatesLess = numOfDatesLess + 1;
                if(trxn.buyOrSell === 'buy'){
                    cumulative = cumulative + trxn.quantity
                } else if(trxn.buyOrSell === 'sell'){
                    cumulative = cumulative - trxn.quantity
                }
            }
        })
        let sortedTrxns = transactions.sort((a,b) => moment(a.date, "YYYY-MM-DD").diff(moment(b.date, "YYYY-MM-DD")))
        let more_trxns = [];
        sortedTrxns.map((trxn, i) => {
            let a = moment(trxn.date, "YYYY-MM-DD");
            let b = moment(itrxn.date, "YYYY-MM-DD");
            if(a.diff(b) > 0){
                more_trxns.push(trxn)
            }
        })
        
        let min_cumulative = 0;
        for(let i = 0; i < sortedTrxns.length; i++){
            let a = moment(sortedTrxns[i].date, "YYYY-MM-DD")
            let b = moment(itrxn.date, "YYYY-MM-DD")

            if(a.diff(b) < 0){
                min_cumulative = min_cumulative  + 1;
            } else {
                break;
            }
        }

        console.log(sortedTrxns)
        console.log(numOfDatesLess)
        if(buyOrSell === 'sell' && getQuantity() > cumulative){
            return false
        }  else {
            return true
        }
    }



    const editTransaction = () => {
        let virtualCurrentCash;
        let virtualCurrentQuantity;

        if(currentTransaction.buyOrSell === 'buy'){
            virtualCurrentCash = currentCash + currentTransaction.amount
            virtualCurrentQuantity = currentQuantity - currentTransaction.quantity
        } else if(currentTransaction.buyOrSell === 'sell'){
            virtualCurrentCash = currentCash - currentTransaction.amount
            virtualCurrentQuantity = currentQuantity + currentTransaction.quantity
        }

    
        if(getAmount() === 0){
            error('please provide sufficient quantity/amount to buy stocks')
        } else if(buyOrSell === 'buy'  && ((getAmount() > virtualCurrentCash) || (currentQuantity - currentTransaction.quantity + getQuantity() < 0) )){
            error('Stock balance cannot be negative')
        } else if(buyOrSell === 'sell' && ((getQuantity() > virtualCurrentQuantity) )){
            error('insuficient stocks to sell')
        } else {
            //alert('transation edited now')
            handleTransactions("edit", {
                company : selectedDataPoint.companyId,
                date : getData("date"),
                amount : getAmount(),
                quantity : getQuantity(),
                id : currentTransaction.id,
                buyOrSell : buyOrSell
            })
            info(` ${buyOrSell} transaction edited`)
            setTransactionInProgress(false)
        }
    }

    const submitTransaction = () => {
        console.log("amount", getAmount())
        if(getAmount() === 0){
            error('please provide sufficient quantity/amount to buy or sell')
        } else if(buyOrSell === 'buy' && (getAmount() > currentCash)){
                error('insuficient amount to buy stocks')
        } else if(buyOrSell === 'sell' && (getQuantity() > currentQuantity)){
                error('insuficient stocks to sell')
        } else if(!resolveCumulative({
            company : selectedCompany.companyId,
            date : getData("date"),
            amount : getAmount(),
            quantity : getQuantity(),
            id : Math.floor(Math.random() * 100000),
            buyOrSell : buyOrSell
        })){
            error('cumulative error')
        } else {
            handleTransactions("add", {
                company : selectedCompany.companyId,
                date : getData("date"),
                amount : getAmount(),
                quantity : getQuantity(),
                id : Math.floor(Math.random() * 100000),
                buyOrSell : buyOrSell
            })
            info(` ${buyOrSell} transaction added`)
            setTransactionInProgress(false)
        }
    }

    const getData = (what) => {

        let cp = 0;
        let dt = "2000-01-01";

        if(priceData){
            cp = priceData[selectedDataPoint][4]
            dt = priceData[selectedDataPoint][0]
        }

        if(what === "date"){
            return dt
        } else if(what === "cp"){
            return cp
        }
    }

    const dataSource = [
        {
            key: '1',
            name: "Market Cap",
            value: "15000.45",
            //value : ''
        },
        {
            key: '2',
            //name: 'Return %',
            //value: this.props.selectedDataElement.yAxes.value,
            name : '52 Week Range',
            value : `${567} , ${1234} `
        },
        {
            key: '3',
            name: 'Date',
            value: getData("date")
            //value : ''
        },
    ];


    const columns = [
        {
            title: 'Details',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Values',
            dataIndex: 'value',
            key: 'value',
        },
    ];


    const dataSource2 = [
        {
            key: '1',
            name: 'Closing Price',
            value: getData("cp")
            //value : ''
        },
        {
            key: '2',
            name: 'P/E Ratio',
            value: 1.5,
            //value : ''
        },
        {
            key: '3',
            name: '6 Month Avg Vol.',
            value: '67,876',
            //value : ''
        }
    ];
    
    const columns2 = [
        {
            title: 'Details',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Values',
            dataIndex: 'value',
            key: 'value',
        },
    ];

    return(
        <div style={{backgroundColor : "#fafafa", borderRadius : "4px", marginTop : "1em", padding : "1em", border : "1px solid #d9d9d9"}}>
                    <div style={{width : "100%", display : "flex", padding : "0.2em 0 0.2em 0" ,alignItems : "center", justifyContent : "space-between"}}>
                        
                            <Title level={4} style={{color : "#2f5164", margin : "0"}}>Company - {selectedCompany.companyName}</Title>
                            <div  style={{color : "#2f5164", marginTop : "0", display : "flex"}}>
                                <StyledChip>Current Quantity : {currentQuantity}</StyledChip>
                                <StyledChip>Current Cash : {currentCash.toFixed(2)}</StyledChip>
                            </div>
                    </div>
                    <div style={{display : "flex", marginTop : "0.5em"}}>
                    <Table  style={{width : "35%", marginRight : "1em"}} size="small" dataSource={dataSource} columns={columns} pagination={false} showHeader={false} bordered/>
                    <Table  style={{width : "35%", marginRight : "1em"}} size="small" dataSource={dataSource2} columns={columns2} pagination={false} showHeader={false} bordered/>
                    <div style={{display : "flex", flexDirection: "column", justifyContent: "space-between" ,width : "30%", borderRadius : "4px" ,border : "none" }}>
                        <div>
                        <Row style={{marginBottom : "0.5em"}}>
                            <Col span={24}>
                                <Radio.Group onChange={(e) => {setBuyOrSell(e.target.value)}} value={buyOrSell} style={{width : "100%"}} defaultValue="buy" buttonStyle="solid">
                                    <Radio.Button style={{width : "50%"}} value="buy">Buy</Radio.Button>
                                    <Radio.Button style={{width : "50%"}} value="sell">Sell</Radio.Button>
                                </Radio.Group>
                            </Col>
                        </Row>
                        <Row style={{marginBottom : "0.5em"}}>
                            <div style={{display : "flex"}}>
                                <Radio.Group onChange={(e) => {setQuantityOrAmount(e.target.value)}} style={{marginRight : "0.5em"}} value={quantityOrAmount} defaultValue="quantity" buttonStyle="solid">
                                    <Radio.Button  value="quantity">Qty</Radio.Button>
                                    <Radio.Button value="amount">Amt</Radio.Button>
                                </Radio.Group>
                                <InputNumber onKeyDown={(e) => {if(e.keyCode === 13){console.log("hi")}}} onChange={(value) => {setStocksWeight(value)}} value={stocksWeight} style={{flexGrow : "1"}} min={1} max={9999999999} defaultValue={3} />
                            </div>
                        </Row>
                        </div>
                        <div style={{width : "100%", display : "flex", justifyContent : "flex-end"}}>
                            <Button style={{marginLeft  : "0.5em"}}>Cancel</Button>
                            {
                                ! transactionInProgress
                                ? <Button onClick={editTransaction} style={{marginLeft  : "0.5em"}} type="danger">Edit Transaction</Button>
                                : <Button onClick={submitTransaction} style={{marginLeft  : "0.5em"}} type="danger">Add Transaction</Button>

                            }
                        </div>
                    </div>    
                </div>
        </div>
    )
}


const StyledChip = styled.div`
    background-color: #d1eeff;
    border-radius: 1em;
    color: #2f5164;
    padding: 2px 15px 2px 15px;
    margin-left: 1em;
    border: 1px solid #2f5164;
    font-size: 0.8em;
`

export default BuySellForm