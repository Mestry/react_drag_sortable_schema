import React, {useContext} from 'react'
import styled from 'styled-components'
import {DatePicker} from 'antd'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'
import * as moment from 'moment'
import {MyDateFormat} from '../../config/config'


const SingleDate = () => {
    const {singleDate, handleSingleDate} = useContext(BackTestContext)
    const {theme} = useContext(MyThemeContext)

        return(
            <StyledDatePicker theme={theme} onChange={date => handleSingleDate(date)}
                value={moment(singleDate, MyDateFormat)}
                format={MyDateFormat}
            />
        )
        
}


const StyledDatePicker = styled(DatePicker)`
    width : 100%;
    & .ant-calendar-picker-input, .ant-calendar-range-picker-separator, .anticon-close-circle{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text}
    };
    & .anticon{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text}
    };
    &  .ant-input {
        color : ${props => props.theme.text};
        border-color : ${props => props.theme.text}
    }
`


export default SingleDate