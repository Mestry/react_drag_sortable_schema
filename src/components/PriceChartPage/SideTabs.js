import React, {useContext, useEffect, useState} from 'react'
import { message, Collapse, Button, Modal, Result, Icon} from 'antd';
import styled from 'styled-components'
import {MyThemeContext} from '../../contexts/MyThemeContext'
import {BackTestContext} from '../../contexts/BackTestContext'
import CompanyList from './CompanyList'
import DateRange from './DateRange'
import SelectFrequency from './SelectFrequency'
import StartingAmountAndCommission from './StartingAmountAndCommission'
import SelectStrategy from './SelectStrategy'
import SingleDate from './SingleDate'
import TransactionsList from './TransactionsList'
import ResultChart from './ResultChart'
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"


const { Panel } = Collapse;

const SideTabs = () => {
    const {theme, currentTheme} = useContext(MyThemeContext)
    const {activeKey, handleActiveKey, loadingBackTestResults ,selectedCompany, strategy, backTestStarted, handleBackTestStarted, getPriceData, handleTransactions, backTestSuccess, handleBackTestSuccess ,backTestResults} = useContext(BackTestContext)
    const [visible, setVisible]  = useState(false)
    const [visible2, setVisible2]  = useState(false)

    const panelChange = (aK) => {
        if(backTestStarted && aK === "1"){
            showModal2();
        } else if( aK === "2") {
            if(backTestStarted){
                showModal2();
            } else if(selectedCompany.companyName === null){
                message.error('select a company')
            } else {
                handleActiveKey(aK)
            }
        } else {
            handleActiveKey(aK)
        }
    }

    useEffect(() => {
        if(loadingBackTestResults === true){
            showModal()
        }
    }, [loadingBackTestResults])

    const showModal = () => {
        setVisible(true)
    };
    
    const  handleOk = e => {
        setVisible(false)
    };
    
    const  handleCancel = e => {
        setVisible(false)
    };

    const showModal2 = () => {
        setVisible2(true)
    };

    const handleOk2 = e => {
        handleTransactions("reset")
        handleBackTestStarted(false)
        setVisible2(false)
        handleActiveKey("1")
    };
    
    const handleCancel2 = e => {
        setVisible2(false)
    };


    const getResultsJSX = () => {
        let results;

        if(loadingBackTestResults){
            results = <Loader
            type="Puff"
            color="#00BFFF"
            height={100}
            width={100}
            timeout={3000} //3 secs
         />
        } else if(backTestSuccess && backTestResults){
            results =   <Result
            status="success"
            title="Back Test Results"
            //subTitle="Order number: 2017182818828182881 Cloud server configuration takes 1-5 minutes, please wait."
            extra={[
                <div style={{display : "flex", flexDirection : "column", width : "100%"}}>
                <StyledResultButton theme={theme} onClick={() => {handleActiveKey("3");handleBackTestSuccess(false)}} type="primary" key="console">
                    Edit Backtest
                    {/* <Icon type="edit" theme="filled" /> */}
                </StyledResultButton>
                <StyledResultButton theme={theme} type="primary" onClick={() => {handleActiveKey("2");handleTransactions("reset");handleBackTestSuccess(false)}} key="buy">
                    Start New Test
                    {/* <Icon type="reload" />                                  */}
                </StyledResultButton>
                <StyledResultButton theme={theme} type="primary" onClick={showModal}>
                    View Results 
                    {/* <Icon type="dashboard" theme="filled" /> */}
                </StyledResultButton>
                </div>
        ]}
        >
            <div style={{display : "flex", flexDirection : "column", color : "black", border : "1px solid #fafafa", borderRadius : "4px"}}>
                <table>
                    <tbody>
                        <tr><td span={12}>Closing Cash Availible</td><td span={12}>{backTestResults.current_cash_value.toFixed(2)}</td></tr>
                        <tr><td span={12}>Value of Stock Held</td><td span={12}>{backTestResults.current_stock_value.toFixed(2)}</td></tr>
                        <tr><td span={12}>Total Invested Amount</td><td span={12}>{backTestResults.total_invested_amount.toFixed(2)}</td></tr>
                        <tr><td span={12}>Annualized Return (XIRR)</td><td span={12}>{backTestResults.xirr.toFixed(2)} %</td></tr>
                    </tbody>
                </table>
            </div>
            <Modal
                title={`Back Test Results - ${selectedCompany.companyName}`}
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
                width={1000}
                >
                <ResultChart />
            </Modal>
        
        </Result>
        } else {
            results = <div style={{color : theme.text}}>No results to display</div>
        }

        return results

    }

    return(
        <React.Fragment>
            <Collapse  accordion activeKey={activeKey} onChange={panelChange}>
                <MyPanel theme={theme} header="Step 1 - Select Backtest Parameters" key="1">
                    <SidePanelItemCon>
                        <SidePanelItemHead>Select Company</SidePanelItemHead>
                        <SidePanelItemBody theme={theme}>
                            <CompanyList />
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                    <SidePanelItemCon>
                        <SidePanelItemHead>Select Date Range</SidePanelItemHead>
                        <SidePanelItemBody theme={theme}>
                            <DateRange />
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                    <SidePanelItemCon>
                        <SidePanelItemHead>Select Frequency</SidePanelItemHead>
                        <SidePanelItemBody theme={theme}>
                            <SelectFrequency />
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                    <SidePanelItemCon>
                        <SidePanelItemHeadDouble><div>Starting Amount</div><div>Commission</div></SidePanelItemHeadDouble>
                        <SidePanelItemBody theme={theme}>
                            <StartingAmountAndCommission />
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                    <SidePanelItemCon>
                        <SidePanelItemHead></SidePanelItemHead>
                        <SidePanelItemBody theme={theme}>
                            <StyledButton theme={theme} currenttheme={currentTheme.currentTheme} onClick={() => {if(selectedCompany.companyName === null){message.error('please select company')} else {handleActiveKey("2")}}} type="default" block>Select Strategy</StyledButton>
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                </MyPanel>
                <MyPanel theme={theme} header="Step 2 - Define Backtest Strategy" key="2">
                    <SidePanelItemCon>
                        <SidePanelItemHead>Select Strategy</SidePanelItemHead>
                        <SidePanelItemBody theme={theme}>
                            <SelectStrategy />
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                    {strategy === "Manual"
                    ?
                    <SidePanelItemCon>
                        <SidePanelItemHead>Select First Investment Date</SidePanelItemHead>
                        <SidePanelItemBody theme={theme}>
                            <SingleDate />
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                    :
                    <div></div>
                    }
                    <SidePanelItemCon>
                        <SidePanelItemHead></SidePanelItemHead>
                        <SidePanelItemBody theme={theme}>
                            <StyledButton theme={theme} currenttheme={currentTheme.currentTheme} onClick={() => {handleBackTestStarted(true);getPriceData();handleActiveKey("3")}} type="default" block>Draw Price Chart</StyledButton>
                        </SidePanelItemBody>
                    </SidePanelItemCon>
                </MyPanel>
                <MyPanel theme={theme} header="Step 3 - Add/Review Transactions" key="3">
                    <TransactionsList />
                </MyPanel>
                <MyPanel theme={theme} header="Step 4 - Backtest Results" key="4">
                    { 
                        getResultsJSX()
                    }
                </MyPanel>
            </Collapse>
            <Modal
                title="Reset Transactions ?"
                visible={visible2}
                onOk={handleOk2}
                onCancel={handleCancel2}
                width={700}
            >
                <p>Do you want to reset all transactions and start new back test ?</p>
            </Modal>
        </React.Fragment>
    )
}


const MyPanel = styled(Panel)`
    color : red;
    &.ant-collapse-item .ant-collapse-content  {
        background-color : ${props => props.theme.bg};
        color : ${props => props.theme.text};
    }
    & .ant-result div svg {
        font-size : 0.5em;
    }
    & .ant-result .ant-result-title {
        color : ${props => props.theme.text};
    }
`

const SidePanelItemCon = styled.div`
    width : 100%;
    margin-bottom : 1em;
    display : flex;
    flex-direction : column;
   
`

const SidePanelItemHead = styled.div`
    margin-bottom : 0.3em;
`
const SidePanelItemHeadDouble = styled.div`
    margin-bottom : 0.3em;
    display : flex;
    justify-content : space-between
`


const SidePanelItemBody = styled.div`
    background-color : ${props => props.theme.bgDark};
    padding : 1em;
    border-radius : 4px;
    
`

const StyledButton = styled(Button)`
    background-color : ${props => props.theme.bgDark};
    color : ${props => props.theme.text};
    border-color : ${props => props.theme.text};
    &:hover{
        color : ${props => { console.log(props.currenttheme);if(props.currenttheme === "dark" ){ return props.theme.bgDark } else { return props.theme.bg }}};
        border : ${props => props.theme.text};
        background-color : ${props => { console.log(props.currenttheme);if(props.currenttheme === "dark" ){ return props.theme.text } else { return props.theme.text }}};
    }
`

const StyledResultButton = styled(Button)`
    margin-top : 1em
    background-color : ${props => props.theme.bg}
    border-color : ${props => props.theme.text}
    color : ${props => props.theme.text};
`

export default SideTabs