import React, {useContext} from 'react'
import styled from 'styled-components'
import {Input, Icon} from 'antd'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'

const StartingAmountAndCommission = () => {
    const {theme} = useContext(MyThemeContext)
    const {startingAmount, handleStartingAmount} = useContext(BackTestContext)
    return(
        <InputsCon theme={theme}>
            <Input style={{marginRight : "5px"}} value={startingAmount} onChange={(e) => handleStartingAmount(e.target.value)} addonBefore={<Icon type="dollar" />} defaultValue="100000" />
            <Input style={{marginLeft : "5px"}} addonAfter={<Icon type="percentage" />} defaultValue="0" />
        </InputsCon>
    )
}

const InputsCon = styled.div`
    display : flex;
    & .ant-input, .ant-input-group-addon{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text};
        border-color : ${props => props.theme.text};
    }
`

export default StartingAmountAndCommission