import React, {useEffect, useCallback, useContext} from 'react'
import SideTabs from './SideTabs'
import styled from 'styled-components'
import Info from './Info'
import PriceGraph from './PriceGraph'
import BuySellForm from './BuySellForm'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'

const PriceChart = () => {
    const {backTestStarted} = useContext(BackTestContext)
    return(
        <PCPCon>
            <div style={{width : "30%", marginRight : "1em"}}>
                <SideTabs />
            </div>
            <div style={{width : "70%", marginLeft : "1em"}}>
                <Info />
                {backTestStarted ? <PriceGraph /> : null}
                {backTestStarted ? <BuySellForm />: null}
            </div>
        </PCPCon>
    )
}

const PCPCon = styled.div`
    width : 100%;
    display : flex;
    padding : 2em;
`


export default PriceChart