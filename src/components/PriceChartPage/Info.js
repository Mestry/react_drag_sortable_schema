import React, {useContext, useState ,useEffect} from 'react'
import styled from 'styled-components'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'

const Info = () => {
    const  {theme, currentTheme} = useContext(MyThemeContext)
    const {activeKey} = useContext(BackTestContext)
    const [sideInfo, setSideInfo] = useState("Select company, date range and initial amount for your backtest")

    useEffect(() => {
        if(activeKey === "1"){
            setSideInfo("Select company, date range and initial amount for your backtest")
          } else if(activeKey === "2"){
            setSideInfo("Select a strategy for your backtest")
          } else if(activeKey === "3"){
            setSideInfo("Add/Review Transactions by clicking on a point in the graph")
          } else if(activeKey === "4"){
            setSideInfo("View Back test Results")
          } else {
            setSideInfo("Backtesting Menu")
          }
    }, [activeKey])

    return(
        <StyledInfoCon theme={theme} currenttheme={currentTheme.currentTheme}><span role="img">👈 </span>&nbsp; &nbsp; {sideInfo}</StyledInfoCon>
    )
}


const StyledInfoCon = styled.div`
    background-color : #fafafa;
    color : ${props => { console.log(props.currenttheme);if(props.currenttheme === "dark" ){ return props.theme.bg } else { return props.theme.text }}};
    padding : 0.5em 2em;
    border-radius : 4px;
    border : 1px solid #d9d9d9;
`

export default Info