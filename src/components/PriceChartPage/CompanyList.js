import React, {useState, useContext} from 'react'
import {AutoComplete, Icon, Input} from 'antd'
import {API} from '../../config/config'
import styled from 'styled-components'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'

const CompanyList = () => {
    const {theme} = useContext(MyThemeContext)
    const {handleSelectedCompany} = useContext(BackTestContext)
    const [searchApiData, setSearchApiData] = useState([])

    //console.log(searchApiData)

    const iconStyle = {
        color : theme.text
    }


    const getSearchData = (search) => {
        fetch(`${API}/companylist`, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          //mode: 'no-cors', // no-cors, *cors, same-origin
          //cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          //credentials: 'omit', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          //redirect: 'follow', // manual, *follow, error
          //referrer: 'no-referrer', // no-referrer, *client
          body: JSON.stringify(
            {
              searchString : search
            } 
          )// body data type must match "Content-Type" header
        }).then((res) => console.log(res.json().then((resh) => {
          console.log(resh)
          setSearchApiData(
            resh.records
          )
        })
        )).catch((err) => console.log(err))
    }

    const getDataSource = () => {
        let dataArray = []
    
        searchApiData.map((sapid) => {
          dataArray.push(sapid[1])
        })
    
        return dataArray
    }

    const getId = (value) => {

        let id;
        searchApiData.map((sapid) => {
          if(value === sapid[1]){
            id = sapid[0]
          }
        })
        return id;
    }

    return(
        <StyledAutoComplete
            theme={theme}
            dataSource={getDataSource()}
            onSearch={(e) => {getSearchData(e)}}
            onSelect = {
                (value) => {
                let cid = getId(value)
                handleSelectedCompany({companyId : cid, companyName : value})
                console.log(cid, value)
                }
            }
            placeholder="search for a company by name"
            filterOption={(inputValue, option) =>
                option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
            }
            >
            <Input
                suffix={
                <Icon style={iconStyle} type="search" />
                }
                />
        </StyledAutoComplete>
    )
}

const StyledAutoComplete = styled(AutoComplete)`
    width : 100%;
    & .ant-select-selection--single {
        background-color : ${props => props.theme.bgDark}
        color : ${props => props.theme.text}
    };
    &  .ant-input {
        color : ${props => props.theme.text};
        border-color : ${props => props.theme.text}
    }
`


export default CompanyList