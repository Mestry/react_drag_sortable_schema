import React, {useContext} from 'react'
import styled from 'styled-components'
import {DatePicker} from 'antd'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'
import * as moment from 'moment'
import {MyDateFormat} from '../../config/config'
const { RangePicker } = DatePicker;


const DateRange = () => {
    const {dateRange, handleDateRange} = useContext(BackTestContext)
    const {theme} = useContext(MyThemeContext)
    return(
        <StyledRangePicker
          theme={theme}
          onChange={date => handleDateRange({startingDate : date[0].format(MyDateFormat), endingDate : date[1].format(MyDateFormat)})}
          value={[moment(dateRange.startingDate, MyDateFormat), moment(dateRange.endingDate, MyDateFormat)]}
          format={MyDateFormat}
        />
    )
}


const StyledRangePicker = styled(RangePicker)`
    width : 100%;
    & .ant-calendar-picker-input, .ant-calendar-range-picker-separator, .anticon-close-circle{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text}
    };
    & .anticon{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text}
    };
    &  .ant-input {
        color : ${props => props.theme.text};
        border-color : ${props => props.theme.text}
    }
`



export default DateRange