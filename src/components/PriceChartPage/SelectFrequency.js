import React, {useContext} from 'react'
import styled from 'styled-components'
import {Select} from 'antd'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'

const { Option } = Select;

const SelectFrequency = () => {
    const {theme} = useContext(MyThemeContext)
    return(
        <StyledSelect theme={theme} defaultValue="weekly">
            <Option value="weekly">weekly</Option>
            <Option value="monthly">monthly</Option>
        </StyledSelect>
    )
}

const StyledSelect = styled(Select)`
    width : 100%;
    & .ant-select-selection{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text};
        border-color : ${props => props.theme.text};
    };
    & .anticon{
        background-color : ${props => props.theme.bgDark};
        color : ${props => props.theme.text}
    };
`

export default SelectFrequency