import React, {useState, useContext, useEffect, useRef, useLayoutEffect} from 'react'
import {Line} from 'react-chartjs-2'
import {Slider, Button, Icon} from 'antd'
import styled from 'styled-components'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'
import * as moment from 'moment'
import 'chartjs-plugin-datalabels'

const PriceGraph = () => {
    const {priceData, singleDate, selectedCompany, handleSelectedDataPoint, transactions} = useContext(BackTestContext)
    const [allDataPoints, setAllDataPoints] = useState([])
    const [allLabels, setAllLabels] = useState([])
    const [dataStart, setDataStart] = useState(0)
    const [dataEnd, setDataEnd] = useState(0)
    const [labelStart, setLabelStart] = useState(0)
    const [labelEnd, setLabelEnd] = useState(0)
    const [ted, setTed] = useState(0)
    const [highlightIndex, setHighlightIndex] = useState(0)
    const [actualHighlightIndex, setActualHighlightIndex] = useState(0)
    const [isRunning, setIsRunning] = useState(false);


    const ccRef = useRef(null);
    let chartReference = useRef(null);


    useInterval(() => {
        if(dataEnd > ted){
            setIsRunning(false)
        }
        setDataEnd(dataEnd + (Math.floor((priceData || []).length/100)))
    }, isRunning ? 15 : null);

    useEffect(() => {
        console.log(actualHighlightIndex)
        handleSelectedDataPoint(actualHighlightIndex)
    }, [actualHighlightIndex, handleSelectedDataPoint])

    useEffect(() => {
        //clearInterval(intervalId);
        let labels = [];
        let dataPoints = [];
        let ted = 0;
        (priceData || []).map((ad) => {
          labels.push(ad[0])
          dataPoints.push(ad[4])
          let m_ad_date = moment(ad[0], "YYYY-MM-DD")
          let m_single_date = moment(singleDate, "YYYY-MM-DD")
          let diff = m_ad_date - m_single_date
          if(diff < 0){
            ted = ted + 1;
          }
        })
        setAllDataPoints(dataPoints)
        setAllLabels(labels)
        setLabelEnd(ted)
        setTed(ted)
        setIsRunning(true)
        console.log("lllllpppppp")
    }, [priceData, singleDate])



    const data = {
        labels : allLabels.slice(labelStart, labelEnd),
        datasets : [{
          label  : selectedCompany.companyName,
          data : allDataPoints.slice(dataStart, dataEnd),
          fill : false,
          borderColor  : "#1890ff"
        }]
    }

    const options={
        animation : {
          duration : 0
        },
         hover: {
            mode: "nearest",
            intersect: false,
            axis : 'x'
          },
        elements: {
          line : {
            backgroundColor : "#1890ff",
            borderColor  : "#1890ff"
          },
          point: {
            hoverRadius : (context) => {
                let hrad = 4;
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate){
                        hrad = 7
                    }
                })
                if(context.dataIndex === highlightIndex){
                    hrad = 7
                }
                return hrad
            },
            radius : (context) => {
                let rad = 0;
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate){
                        rad = 7
                    }
                })
                if(context.dataIndex === highlightIndex){
                    rad = 7
                }
                return rad
            },
            borderWidth : 0,
            display: true,
            pointStyle : (context) => {
            let ps = 'circle';
            let contextCompany = context.dataset.label
            let contextDataIndex = context.dataIndex
            let contextDate = context.chart.config.data.labels[contextDataIndex]
            transactions.map((trxn) => {
                if(trxn.date === contextDate ){
                    ps = 'triangle'
                }
            })
            return ps
            },
            backgroundColor : (context) => {
            let bc = '#1890ff';
            let contextCompany = context.dataset.label
            let contextDataIndex = context.dataIndex
            let contextDate = context.chart.config.data.labels[contextDataIndex]
            transactions.map((trxn) => {
                if(trxn.date === contextDate && trxn.buyOrSell === 'buy'){
                    bc = '#148f42'
                } else if(trxn.date === contextDate && trxn.buyOrSell === 'sell') {
                    bc = '#ff003c'
                }
            })
            return bc
            }
        }
        },
        plugins : {
            datalabels: {
                clamp : true,
                anchor : 'center',
                align : 'end',
                offset : 20,
                color: '#eee',
                borderRadius : 20,
                font : {
                weight : 'bold',
                },
                borderWidth : 2,
                backgroundColor : (context) => {
                let color = '#ff003c';
                let contextCompany = context.dataset.label
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate  && trxn.buyOrSell === 'buy'){
                    color = '#148f42'
                    } else if(trxn.date === contextDate && trxn.buyOrSell === 'sell'){
                    color = '#ff003c'
                    }
                })
                return color
                },
                rotation : 0,
                display : (context) => {
                let found = false;
                let contextCompany = context.dataset.label
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate ){
                    found = true
                    }
                })
                return found
                },
                formatter: (value, context) => {
                let ren = 'B';
                let contextCompany = context.dataset.label
                let contextDataIndex = context.dataIndex
                let contextDate = context.chart.config.data.labels[contextDataIndex]
                transactions.map((trxn) => {
                    if(trxn.date === contextDate && trxn.buyOrSell === 'buy'){
                    
                    ren = 'B'
                    } else if(trxn.date === contextDate && trxn.buyOrSell === 'sell'){
                    ren = 'S'
                    }
                })
                return ren
                },
                listeners : {
                click : function(context){
                    alert('clicked')
                }
                }
            }
        },
        tooltips : {
          mode : 'index',
          intersect : false,
    
        },
        onHover : (a,b,c) => {
          const god = ccRef.current.getBoundingClientRect()
          const godX = god.left
          const meX = a.clientX-godX
          const ctx = ccRef.current.getContext("2d");
          ctx.clearRect(0, 0, god.width, god.height);
          ctx.beginPath();
          ctx.moveTo(meX, 0);
          ctx.lineTo(meX, god.height);
          ctx.strokeStyle = "#333";
          ctx.stroke();
        },
        onClick : (event, elems) => {
            console.log(event,elems)
            let chartElem = elems
            console.log(chartElem)
            if(chartElem !== undefined && chartElem.length !== 0){
            setActualHighlightIndex(chartElem[0]._index + dataStart)
            setHighlightIndex(chartElem[0]._index)
            }
        },
        scales: {
            xAxes: [
                {
                position: 'bottom',
                id: 'bottom-x-axis',
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: "Date"
                },
                ticks : {
                    callback : function(value, index, values){
                    return moment(value, "YYYY-MM-DD").format("MMM-YY");
                    }
                }
                }
            ],
            yAxes: [
                {
                position: 'left',
                id: 'left-y-axis',
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: "Closing Price"
                }
                }
            ]
        },
    }

    return(
        <StyledPriceChartCon tabIndex="0"  onKeyDown={
            (e) => {
              if(e.keyCode === 39){
                setHighlightIndex(highlightIndex + 1)
                setActualHighlightIndex(actualHighlightIndex + 1)
              } else if(e.keyCode === 37){
                setHighlightIndex(highlightIndex - 1)
                setActualHighlightIndex(actualHighlightIndex - 1)
              } else if(e.keyCode === 13){
                // this.setState({
                //   displayBuySellForm : true
                // })
              }
            }}
        >
            <div>
                <div style={{position : "relative"}}>
                <canvas ref={ccRef}  style={{position : "absolute"}} width={800} height={400} />
                <div style={{width : "800px", position : "relative"}}>
                    <Line ref={(reference) => chartReference = reference }
                        width={800}
                        height={400}
                        data={data}
                        options={options}
                    />
                </div>
                </div>
                <Slider
                    style={{width : "800px"}}
                    range onChange={(val) => {
                        setLabelStart(val[0]);
                        setLabelEnd(val[1]);
                        setDataStart(val[0]);
                        setDataEnd(val[1]);
                    }} 
                    value={[dataStart, dataEnd]}
                    defaultValue={[0, 4000]}
                    min={0}
                    max={priceData ? priceData.length : 1000} 
                    tipFormatter={(value) => {
                        return (
                            allLabels[value]
                        )
                    }}
                    />
            </div>
            <div style={{display : "flex", flexDirection : "column"}}>
                <Button onClick={() => {
                    setHighlightIndex(highlightIndex - 1)
                    setActualHighlightIndex(actualHighlightIndex - 1)
                }}>
                <Icon className="btn-icon-lr"  type="left-circle" />
                </Button>
                <Button style={{marginTop : "0.5em"}} onClick={() => {
                    setHighlightIndex(highlightIndex + 1)
                    setActualHighlightIndex(actualHighlightIndex + 1)
                }}>
                <Icon className="btn-icon-lr"  type="right-circle" />
                </Button>
            </div>
        </StyledPriceChartCon>
    )
}


const StyledPriceChartCon = styled.div`
    margin-top : 1em;
    width : 100%;
    padding-top : 0.5em;
    background-color: #fafafa;
    display : flex;
    align-items: center;
    justify-content: center;
    border-radius: 4px;
    border : 1px solid #d9d9d9;

`

function useInterval(callback, delay) {
    const savedCallback = useRef();
  
    // Remember the latest callback.
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);
  
    // Set up the interval.
    useEffect(() => {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }, [delay]);
  }

export default PriceGraph