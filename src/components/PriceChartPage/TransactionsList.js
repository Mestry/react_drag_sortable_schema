import React, { useContext } from 'react'
import styled from 'styled-components'
import { MyThemeContext } from '../../contexts/MyThemeContext'
import { BackTestContext } from '../../contexts/BackTestContext'
import {message, Row, Col, Icon, Button} from 'antd'

const TransactionsList = () => {
    const {theme, currentTheme} = useContext(MyThemeContext)
    const {handleActiveKey, transactions, handleTransactions} = useContext(BackTestContext)

    return(
        <React.Fragment>
            <StyledHeadRow theme={theme} currenttheme={currentTheme.currentTheme}>
                <Col span={10}>Date</Col>
                <Col span={6}>Amount</Col>
                <Col span={6}>Quantity</Col>
                <Col span={2}><Icon type="delete" theme="filled" /></Col>
            </StyledHeadRow>
            {transactions.length === 0 
            ?
            <StyledRow theme={theme} >
                <Col span={24}>No Transactions to display</Col>
            </StyledRow>
            :
            transactions.map((trxn, i) => {
                return(
                    <StyledRow theme={theme}  key={i} style={{borderLeftWidth : "5px",
                    borderLeftColor : trxn.buyOrSell === 'buy' ? '#44c744' : 'red'
                    }}>
                        <Col span={10}>{trxn.date}</Col>
                        <Col span={6}>{trxn.buyOrSell === 'buy' ?  trxn.amount.toFixed(0) : `-${trxn.amount.toFixed(0)}`}</Col>
                        <Col span={6}>{trxn.buyOrSell === 'buy' ?  trxn.quantity : `-${trxn.quantity}`}</Col>
                        <Col onClick={() => handleTransactions("delete", trxn)} style={{cursor : "pointer"}} span={2}><Icon type="delete" theme="filled" /></Col>
                    </StyledRow>
                )
            })
            }
            <div style={{width : "100%", marginTop : "1em", display : "flex", justifyContent : "flex-end"}}>
                <Button onClick={() => {handleTransactions("reset")}} style={{marginLeft  : "0.5em"}}>Reset Transactions</Button>
                <Button onClick={() => {
                    if(transactions.length > 0){
                        handleTransactions("send");
                        //this.showModal();
                        handleActiveKey("4")
                    } else {
                        message.error('no transactions added')
                    }
                }} style={{marginLeft  : "0.5em"}} type="danger">Run BackTest</Button>
            </div>
        </React.Fragment>
    )
}

const StyledHeadRow = styled(Row)`
    background-color : ${props => { console.log(props.currenttheme);if(props.currenttheme === "dark" ){ return props.theme.text } else { return props.theme.bgDark }}};
    color : ${props => { console.log(props.currenttheme);if(props.currenttheme === "dark" ){ return props.theme.bg } else { return props.theme.text }}};
    padding : 0.3em 0.5em 0.3em 0.5em;
    border-radius : 2px;
    width : 100%;
`

const StyledRow = styled(Row)`
    background-color : ${props => props.theme.bg};
    color : ${props => props.theme.text};
    padding : 0.3em 0.5em 0.3em 0.5em;
    border-radius : 2px;
    margin-top : 0.5em;
    border : 1px solid ${props => props.theme.text};
    width : 100%;
`

export default TransactionsList